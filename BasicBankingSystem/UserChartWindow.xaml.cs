﻿// <copyright file="UserChartWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System.Collections.Generic;
    using System.Windows;
    using DatabaseClassLibrary;
    using ViewModels;

    /// <summary>
    /// Interaction logic for UserChartWindow.xaml
    /// </summary>
    public partial class UserChartWindow : Window
    {
        /// <summary>
        /// ViewModel of the chart
        /// </summary>
        private ChartViewModel _vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserChartWindow"/> class.
        /// </summary>
        public UserChartWindow()
        {
            this.InitializeComponent();
            IBbsRepo repo = new BbsRepo();

            this._vm = new ChartViewModel(repo);
            this.DataContext = this._vm;
        }
    }
}
