﻿// <copyright file="ManageItem.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// Interaction logic for ManageItem.xaml
    /// </summary>
    public partial class ManageItem : Window
    {
        private ManageItemViewModel _vm;
        private string _type;
        private bool _isBoss;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageItem"/> class.
        /// </summary>
        /// <param name="type"> The type of the BbsLogic. </param>
        /// <param name="logID"> The ID of the logged in user. </param>
        /// <param name="isBoss"> Indicates wheter user is boss or not. </param>
        public ManageItem(string type, int logID, bool isBoss)
        {
            this.InitializeComponent();
            this.LogID = logID;
            this._type = type;
            this._vm = new ManageItemViewModel(type, new BbsRepo());
            this.DataContext = this._vm;
            this.KeyDown += this.ManageItem_KeyDown;
            this._isBoss = isBoss;
            if (isBoss)
            {
                this.Background = Brushes.LightSeaGreen;
            }
            else
            {
                this.Background = Brushes.LightGreen;
            }
        }

        /// <summary>
        /// Gets the ID of the logged in user.
        /// </summary>
        /// <value>
        /// The ID of the logged in user.
        /// </value>
        public int LogID { get; private set; }

        private void ManageItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.SearchInputText.Text != null && e.Key == Key.Enter)
            {
                this._vm.SearchItem(this.SearchInputText.Text);
            }
        }

        private void AddNewButton_Click(object sender, RoutedEventArgs e)
        {
            new MoveWindow(this._isBoss, "add", this._type, this._vm.SelectItem, this.LogID, this._vm.GetAllItem.Count + 1).ShowDialog();
            this._vm.ListUpdate();
            this._vm.OPC("GetAllItem");
        }

        private void ViewerButton_Click(object sender, RoutedEventArgs e)
        {
            if (this._vm.SelectItem != null)
            {
                new MoveWindow(this._isBoss, "view", this._type, this._vm.SelectItem, this.LogID, -1).ShowDialog();
                this._vm.ListUpdate();
            }
            else
            {
                MessageBox.Show("No item has been selected! Please select one from the list", "Warning", MessageBoxButton.OK);
            }
        }
    }
}
