﻿// <copyright file="MoveWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// Interaction logic for MoveWindow.xaml
    /// </summary>
    public partial class MoveWindow : Window
    {
        private string _type;

        private string _move;

        private IBbsRepo _repo;

        private IBbsUserLogic _userLogic;

        private IBbsLoanLogic _loanLogic;

        private IBbsCustomerLogic _customerLogic;

        private IBbsContractLogic _contractLogic;

        private UserViewModel _userVM;

        private LoanViewModel _loanVM;

        private ContractViewModel _contractVM;

        private CustomerViewModel _customerVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoveWindow"/> class.
        /// </summary>
        /// <param name="isBoss"> Is the user a boss or not. </param>
        /// <param name="move"> The type of the window. </param>
        /// <param name="type"> The type of the BbsLogic. </param>
        /// <param name="item"> The selected BbsLogic item. </param>
        /// <param name="logID"> The ID of the logged in user. </param>
        /// <param name="newID"> The ID of the new user. </param>
        public MoveWindow(bool isBoss, string move, string type, object item, int logID, int newID)
        {
            this.LogID = logID;
            this._type = type;
            this._repo = new BbsRepo();
            this.InitializeComponent();
            if (isBoss)
            {
                this.Background = Brushes.LightSeaGreen;
            }
            else
            {
                this.Background = Brushes.LightGreen;
            }

            if (type == "User")
            {
                this._userLogic = new BbsUserLogic(this._repo);
                User user = item as User;
                this._userVM = new UserViewModel(user, this._userLogic.GetAllUsers().Count() + 1, move);
                this.UserMoveItem(move);
            }
            else if (type == "Loan")
            {
                Loan loan = item as Loan;
                this._loanVM = new LoanViewModel(loan);
                this.LoanMoveItem(move);
            }
            else if (type == "Client")
            {
                Customer customer = item as Customer;
                this._customerVM = new CustomerViewModel(customer);
                this.CustomerMoveItem(move);
            }
            else if (type == "Contract")
            {
                Contract contract = item as Contract;
                this._contractVM = new ContractViewModel(contract);
                this.ContractMoveItem(move);
            }
        }

        /// <summary>
        /// Gets or sets the ID of the logged in user.
        /// </summary>
        /// <value>
        /// The ID of the logged in user.
        /// </value>
        public int LogID { get; set; }

        private void ContractMoveItem(string move)
        {
            if (move == "add")
            {
                this.Title = this._type + " add";
                this.contractAddGrid.Visibility = Visibility.Visible;
            }
            else
            {
                this.Title = this._type + " view";
                this.contractDataGrid.Visibility = Visibility.Visible;
            }

            this.DataContext = this._contractVM;
        }

        private void CustomerMoveItem(string move)
        {
            if (move == "add")
            {
                this.Title = this._type + " add";
                this.customerAddGrid.Visibility = Visibility.Visible;
            }
            else
            {
                this.Title = this._type + " view";
                this.customerDataGrid.Visibility = Visibility.Visible;
            }

            this.DataContext = this._customerVM;
        }

        private void LoanMoveItem(string move)
        {
            if (move == "add")
            {
                this.Title = this._type + " add";
                this.loanAddGrid.Visibility = Visibility.Visible;
            }
            else
            {
                this.Title = this._type + " view";
                this.loanDataGrid.Visibility = Visibility.Visible;
            }

            this.DataContext = this._loanVM;
        }

        private void UserMoveItem(string move)
        {
            if (move == "add")
            {
                this.Title = this._type + " add";
                this._userLogic = new BbsUserLogic(this._repo);
                this.userAddGrid.Visibility = Visibility.Visible;
            }
            else
            {
                this.Title = this._type + " view";
                this.userDataGrid.Visibility = Visibility.Visible;
            }

            this.DataContext = this._userVM;
        }

        private void DataSettingButton_Click(object sender, RoutedEventArgs e)
        {
            this._move = "setting";
            if (this._type == "User")
            {
                this.userDataGrid.Visibility = Visibility.Hidden;
                this.userSettingGrid.Visibility = Visibility.Visible;
            }
            else if (this._type == "Loan")
            {
                this.loanDataGrid.Visibility = Visibility.Hidden;
                this.loanSettingGrid.Visibility = Visibility.Visible;
            }
            else if (this._type == "Client")
            {
                this.customerDataGrid.Visibility = Visibility.Hidden;
                this.customerSettingGrid.Visibility = Visibility.Visible;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string error_message = null;
            if (this._type == "User")
            {
                this._userLogic = new BbsUserLogic(this._repo);
                if (this._move != "setting")
                {
                    error_message = this._userLogic.CreateUser(
                       this.userAddNameText.Text,
                       this.userAddPasswordText.Password,
                       this.userAddCreationDateText.Text,
                       this.userAddSalaryText.Text,
                       this.userAddPhoneNumberText.Text,
                       this.userAddAddressText.Text,
                       this.userAddBossCheckBox.IsChecked == true ? "Y" : "N");
                }
                else
                {
                    error_message = this._userLogic.UpdateUser(
                        this._userVM.ID,
                        this.userSettingNameText.Text,
                        this.userSettingPasswordText.Password == string.Empty ? this._userVM.Password : this.userSettingPasswordText.Password,
                        this.userSettingSalaryText.Text,
                        this.userSettingPhoneNumberText.Text,
                        this.userSettingAddressText.Text,
                        this.userSettingBossCheckBox.IsChecked == true ? "Y" : "N");
                }
            }
            else if (this._type == "Loan")
            {
                this._loanLogic = new BbsLoanLogic(this._repo);
                if (this._move != "setting")
                {
                    error_message = this._loanLogic.CreateLoan(
                        this.loanAddNameText.Text,
                        this.loanAddDescriptionText.Text,
                        this.loanAddMaxAmountText.Text,
                        this.loanAddMaxDurationText.Text,
                        this.loanAddMinSalaryText.Text,
                        this.loanAddTHMText.Text);
                }
                else
                {
                    error_message = this._loanLogic.UpdateLoan(
                        this._loanVM.ID,
                        this.loanSettingNameText.Text,
                        this.loanSettingDescriptionText.Text,
                        this.loanSettingMaxAmountText.Text,
                        this.loanSettingMaxDurationText.Text,
                        this.loanSettingMinSalaryText.Text,
                        this.loanSettingTHMText.Text);
                }
            }
            else if (this._type == "Client")
            {
                this._customerLogic = new BbsCustomerLogic(this._repo);
                if (this._move != "setting")
                {
                    error_message = this._customerLogic.CreateCustomer(
                        this.customerAddNameText.Text,
                        this.customerAddAddressText.Text,
                        this.customerAddBirthDateText.Text,
                        this.customerAddCountryText.Text,
                        this.customerAddSalaryText.Text);
                }
                else
                {
                    error_message = this._customerLogic.UpdateCustomer(
                        this._customerVM.ID,
                        this.customerSettingNameText.Text,
                        this.customerSettingAddressText.Text,
                        this.customerSettingBirthDateText.Text,
                        this.customerSettingCountryText.Text,
                        this.customerSettingSalaryText.Text);
                }
            }
            else if (this._type == "Contract")
            {
                this._contractLogic = new BbsContractLogic(this._repo);
                error_message = this._contractLogic.CreateContract(
                    this.contractAddCustomerIDText.Text,
                    this.contractAddProductIDText.Text,
                    this.LogID,
                    this.contractAddAmountText.Text,
                    this.contractAddDurationText.Text);
            }

            if (error_message != null)
            {
                MessageBox.Show(error_message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                this.Close();
            }
        }

        private void CustomerDataDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            this._customerLogic = new BbsCustomerLogic(this._repo);
            this._customerLogic.DeleteCustomer(this._customerVM.ID);
            this.Close();
        }
    }
}
