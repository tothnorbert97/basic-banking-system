﻿// <copyright file="Menu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private bool _isBoss;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="isBoss"> The job title of the user. </param>
        /// <param name="logID"> The ID of the logged in user. </param>
        public Menu(bool isBoss, int logID)
        {
            this.InitializeComponent();
            this.LogID = logID;
            this.DataContext = new MenuViewModel(isBoss);
            this._isBoss = isBoss;
            if (isBoss)
            {
                this.Background = Brushes.LightSeaGreen;
            }
            else
            {
                this.Background = Brushes.LightGreen;
            }
        }

        /// <summary>
        /// Gets the ID of the logged in user.
        /// </summary>
        /// <value>
        /// The ID of the logged in user.
        /// </value>
        public int LogID { get; private set; }

        private void ManageUsersButton_Click(object sender, RoutedEventArgs e)
        {
            new ManageItem("User", this.LogID, this._isBoss).Show();
        }

        private void ManagaClientsButton_Click(object sender, RoutedEventArgs e)
        {
            new ManageItem("Client", this.LogID, this._isBoss).Show();
        }

        private void ManageLoansButton_Click(object sender, RoutedEventArgs e)
        {
            new ManageItem("Loan", this.LogID, this._isBoss).Show();
        }

        private void ManageContractsButton_Click(object sender, RoutedEventArgs e)
        {
            new ManageItem("Contract", this.LogID, this._isBoss).Show();
        }

        private void ShowChartButton_Click(object sender, RoutedEventArgs e)
        {
            UserChartWindow uchw = new UserChartWindow();
            uchw.ShowDialog();
        }

        private void BtnSignout_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }
    }
}
