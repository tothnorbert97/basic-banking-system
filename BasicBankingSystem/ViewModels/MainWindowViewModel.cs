﻿// <copyright file="MainWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// ViewModel of the MainWindow.
    /// </summary>
    public class MainWindowViewModel
    {
        private IBbsUserLogic _logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        /// <param name="logic"> Created logic. </param>
        public MainWindowViewModel(IBbsUserLogic logic)
        {
            this._logic = logic;
        }

        /// <summary>
        /// Gets or sets the username of the user.
        /// </summary>
        /// <value>
        /// The username of the user.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the username of the user.
        /// </summary>
        /// <value>
        /// The username of the user.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Check if username and password are correct.
        /// </summary>
        /// <param name="username"> The given username. </param>
        /// <param name="password"> The given password. </param>
        /// <returns> Returns true if username and password is correct, returns false if username or password is incorrect. </returns>
        public bool CanLogIn(string username, string password)
        {
            return this._logic.LoginUser(int.Parse(username), password) == 1;
        }

        /// <summary>
        /// Check if the user is boss.
        /// </summary>
        /// <param name="username"> The given username. </param>
        /// <returns> Returns true if the user is the boss, returns false if the user is not the boss. </returns>
        public bool IsBoss(string username)
        {
            return this._logic.GetUserByID(int.Parse(username)).IsBoss == "Y";
        }
    }
}
