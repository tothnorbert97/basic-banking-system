﻿// <copyright file="MenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    /// <summary>
    /// ViewModel of the Menu.
    /// </summary>
    public class MenuViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        /// <param name="isBoss"> The job title of the user. </param>
        public MenuViewModel(bool isBoss)
        {
            this.IsBoss = isBoss;
        }

        /// <summary>
        /// Gets a value indicating whether gets the job title of the user.
        /// </summary>
        /// <value>
        /// The job title of the user.
        /// </value>
        public bool IsBoss { get; }
    }
}
