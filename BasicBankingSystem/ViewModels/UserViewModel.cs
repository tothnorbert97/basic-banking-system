﻿// <copyright file="UserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using DatabaseClassLibrary;

    /// <summary>
    /// ViewModel of the MoveWindow.
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        /// <param name="user"> The selected user. </param>
        /// <param name="newID"> The ID of the user. </param>
        /// <param name="move"> Type of window. </param>
        public UserViewModel(User user, int newID, string move)
        {
            if (move != "view")
            {
                this.ID = newID;
            }
            else
            {
                if (user != null)
                {
                    this.ID = int.Parse(user.UserID.ToString());
                    this.Password = user.UserPW;
                    this.Name = user.Name;
                    this.Salary = int.Parse(user.Salary.ToString());
                    this.Phone = user.Phone;
                    this.Address = user.Addr;
                    this.Isboss = user.IsBoss;
                    this.Hiredate = user.HireDate.ToShortDateString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the ID of the user.
        /// </summary>
        /// <value>
        /// The ID of the user.
        /// </value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the password of the user.
        /// </summary>
        /// <value>
        /// The password of the user.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the hiredate of the user.
        /// </summary>
        /// <value>
        /// The hiredate of the user.
        /// </value>
        public string Hiredate { get; set; }

        /// <summary>
        /// Gets or sets the salary of the user.
        /// </summary>
        /// <value>
        /// The salary of the user.
        /// </value>
        public int Salary { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the user.
        /// </summary>
        /// <value>
        /// The phone number of the user.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the address of the user.
        /// </summary>
        /// <value>
        /// The address of the user.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the job title of the user.
        /// </summary>
        /// <value>
        /// The job title of the user.
        /// </value>
        public string Isboss { get; set; }
    }
}
