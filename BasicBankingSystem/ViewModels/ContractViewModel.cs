﻿// <copyright file="ContractViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using DatabaseClassLibrary;

    /// <summary>
    /// ViewModel of the MoveWindow.
    /// </summary>
    public class ContractViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractViewModel"/> class.
        /// </summary>
        /// <param name="contract"> The selected contract. </param>
        public ContractViewModel(Contract contract)
        {
            if (contract != null)
            {
                this.Custid = int.Parse(contract.CustomerID.ToString());
                this.Productid = int.Parse(contract.ProductID.ToString());
                this.Userid = int.Parse(contract.UserID.ToString());
                this.Amount = int.Parse(contract.Amount.ToString());
                this.Duration = int.Parse(contract.Duration.ToString());
                this.CreationDate = contract.CreationDate.ToShortDateString();
            }
        }

        /// <summary>
        /// Gets or sets the ID of the customer.
        /// </summary>
        /// <value>
        /// The ID of the customer.
        /// </value>
        public int Custid { get; set; }

        /// <summary>
        /// Gets or sets the ID of the product.
        /// </summary>
        /// <value>
        /// The ID of the product.
        /// </value>
        public int Productid { get; set; }

        /// <summary>
        /// Gets or sets the ID of the user.
        /// </summary>
        /// <value>
        /// The ID of the user.
        /// </value>
        public int Userid { get; set; }

        /// <summary>
        /// Gets or sets the amount of the contract.
        /// </summary>
        /// <value>
        /// The amount of the contract..
        /// </value>
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the duration of the contract.
        /// </summary>
        /// <value>
        /// The duration of the contract.
        /// </value>
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the creation date of the contract.
        /// </summary>
        /// <value>
        /// The creation date of the contract.
        /// </value>
        public string CreationDate { get; set; }
    }
}
