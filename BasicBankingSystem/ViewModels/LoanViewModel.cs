﻿// <copyright file="LoanViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using DatabaseClassLibrary;

    /// <summary>
    /// ViewModel of the MoveWindow.
    /// </summary>
    public class LoanViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanViewModel"/> class.
        /// </summary>
        /// <param name="loan"> The selected loan. </param>
        public LoanViewModel(Loan loan)
        {
            if (loan != null)
            {
                this.ID = int.Parse(loan.ProductID.ToString());
                this.Name = loan.Name;
                this.Descr = loan.Descr;
                this.Maxamount = int.Parse(loan.MaxAmount.ToString());
                this.Maxdur = int.Parse(loan.MaxDur.ToString());
                this.Minsal = int.Parse(loan.MinSal.ToString());
                this.Loanapr = int.Parse(loan.LoanAPR.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the name of the loan.
        /// </summary>
        /// <value>
        /// The name of the loan.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the loan.
        /// </summary>
        /// <value>
        /// The description of the loan.
        /// </value>
        public string Descr { get; set; }

        /// <summary>
        /// Gets or sets the max amount of the loan.
        /// </summary>
        /// <value>
        /// The max amount of the loan.
        /// </value>
        public int Maxamount { get; set; }

        /// <summary>
        /// Gets or sets the max duration of the loan.
        /// </summary>
        /// <value>
        /// The max duration of the loan.
        /// </value>
        public int Maxdur { get; set; }

        /// <summary>
        /// Gets or sets the minimum salary of the user.
        /// </summary>
        /// <value>
        /// The minimum salary of the loan.
        /// </value>
        public int Minsal { get; set; }

        /// <summary>
        /// Gets or sets the annual percentage rate of the loan.
        /// </summary>
        /// <value>
        /// The annual percentage rate of the loan.
        /// </value>
        public int Loanapr { get; set; }

        /// <summary>
        /// Gets or sets the ID of the loan.
        /// </summary>
        /// <value>
        /// The ID of the loan.
        /// </value>
        public int ID { get; set; }
    }
}
