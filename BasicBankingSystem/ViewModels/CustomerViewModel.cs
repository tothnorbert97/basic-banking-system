﻿// <copyright file="CustomerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System;
    using DatabaseClassLibrary;

    /// <summary>
    /// ViewModel of the MoveWindow.
    /// </summary>
    public class CustomerViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerViewModel"/> class.
        /// </summary>
        /// <param name="customer"> The selected customer. </param>
        public CustomerViewModel(Customer customer)
        {
            if (customer != null)
            {
                this.ID = int.Parse(customer.CustomerID.ToString());
                this.Name = customer.Name;
                this.Address = customer.Addr;
                this.Birthday = customer.BirthDate.ToShortDateString();
                this.Accnum = customer.AccNum;
                this.Salary = int.Parse(customer.Salary.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>
        /// The name of the customer.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address of the customer.
        /// </summary>
        /// <value>
        /// The address of the customer.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the birth day of the customer.
        /// </summary>
        /// <value>
        /// The birth day of the customer.
        /// </value>
        public string Birthday { get; set; }

        /// <summary>
        /// Gets or sets the account number of the customer.
        /// </summary>
        /// <value>
        /// The account number of the customer.
        /// </value>
        public string Accnum { get; set; }

        /// <summary>
        /// Gets or sets the salary of the customer.
        /// </summary>
        /// <value>
        /// The salary of the customer.
        /// </value>
        public int Salary { get; set; }

        /// <summary>
        /// Gets or sets the ID of the customer.
        /// </summary>
        /// <value>
        /// The ID of the customer.
        /// </value>
        public int ID { get; set; }
    }
}
