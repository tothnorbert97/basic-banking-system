﻿// <copyright file="ChartViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// ViewModel for Chart.
    /// </summary>
    public class ChartViewModel
    {
        private IBbsRepo _repo;

        private IBbsUserLogic _userLogic;

        private List<KeyValuePair<string, int>> _valueList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartViewModel"/> class.
        /// </summary>
        /// <param name="repo"> Repo of data. </param>
        public ChartViewModel(IBbsRepo repo)
        {
            this._repo = repo;
            this._userLogic = new BbsUserLogic(this._repo);
            this.ValueList = this._userLogic.GetUserChartData();
        }

        /// <summary>
        /// Gets or sets value list of chart.
        /// </summary>
        /// <value>
        /// Value list of chart.
        /// </value>
        public List<KeyValuePair<string, int>> ValueList
        {
            get
            {
                return this._valueList;
            }

            set
            {
                this._valueList = value;
            }
        }
    }
}
