﻿// <copyright file="ManageItemViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// ViewModel of the ManageItemWindow.
    /// </summary>
    public class ManageItemViewModel : INotifyPropertyChanged
    {
        private string _type;

        private IBbsRepo _repo;

        private IBbsContractLogic _contractLogic;

        private IBbsUserLogic _userLogic;

        private IBbsCustomerLogic _customerLogic;

        private IBbsLoanLogic _loanLogic;

        private ObservableCollection<object> _getAllItem;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageItemViewModel"/> class.
        /// </summary>
        /// <param name="type"> The type of the window. </param>
        /// <param name="repo"> The created repository. </param>
        public ManageItemViewModel(string type, IBbsRepo repo)
        {
            this.GetAllItem = new ObservableCollection<object>();
            this._type = type;
            this._repo = repo;
            this.ListUpdate();
        }

        /// <summary>
        /// Indicate changes;
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public object SelectItem { get; set; }

        /// <summary>
        /// Gets titletText
        /// </summary>
        /// <value>
        /// TitletText
        /// </value>
        public string TitleText
        {
            get
            {
                return "Manage " + this._type.ToLower() + "s";
            }
        }

        /// <summary>
        /// Gets addText
        /// </summary>
        /// <value>
        /// AddText
        /// </value>
        public string AddText
        {
            get
            {
                return "Add new " + this._type.ToLower();
            }
        }

        /// <summary>
        /// Gets viewText
        /// </summary>
        /// <value>
        /// ViewText
        /// </value>
        public string ViewText
        {
            get
            {
                return "View " + this._type.ToLower();
            }
        }

        /// <summary>
        /// Gets all items of the selected logic.
        /// </summary>
        /// <value>
        /// All item of the selected logic.
        /// </value>
        public ObservableCollection<object> GetAllItem
        {
            get
            {
                return this._getAllItem;
            }

            private set
            {
                this._getAllItem = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Get all user to the GetAllItem list.
        /// </summary>
        public void CreateUserList()
        {
            IEnumerable<User> list = this._userLogic.GetAllUsers();
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var user in list)
            {
                this.GetAllItem.Add(user);
            }
        }

        /// <summary>
        /// Search in the selected logic.
        /// </summary>
        /// <param name="search"> The searched item. </param>
        public void SearchItem(string search)
        {
            this.GetAllItem = null;

            if (this._type == "User")
            {
                this.SearchUser(search);
            }
            else if (this._type == "Client")
            {
                this.SearchCustomer(search);
            }
            else if (this._type == "Loan")
            {
                this.SearchLoan(search);
            }
            else if (this._type == "Contract")
            {
                this.SearchContract(search);
            }
        }

        /// <summary>
        /// Indicate to the changed property.
        /// </summary>
        /// <param name="propertyName"> The changed property. </param>
        public void OPC([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Update the GetAllItem list.
        /// </summary>
        public void ListUpdate()
        {
            if (this._type == "User")
            {
                this._repo = new BbsRepo();
                this._userLogic = new BbsUserLogic(this._repo);
                this.CreateUserList();
            }
            else if (this._type == "Loan")
            {
                this._repo = new BbsRepo();
                this._loanLogic = new BbsLoanLogic(this._repo);
                this.CretaeLoanList();
            }
            else if (this._type == "Client")
            {
                this._repo = new BbsRepo();
                this._customerLogic = new BbsCustomerLogic(this._repo);
                this.CreateCustomerList();
            }
            else
            {
                this._repo = new BbsRepo();
                this._contractLogic = new BbsContractLogic(this._repo);
                this.CreateContractList();
            }
        }

        /// <summary>
        /// Search the searched contract.
        /// </summary>
        /// <param name="search"> The searched contract. </param>
        private void SearchContract(string search)
        {
            IEnumerable<Contract> list = this._contractLogic.GetAllContracts().Where(x => x.ContractID.ToString() == search);
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var item in list)
            {
                this.GetAllItem.Add(item);
            }
        }

        /// <summary>
        /// Search the searched loan.
        /// </summary>
        /// <param name="search"> The searched loan. </param>
        private void SearchLoan(string search)
        {
            IEnumerable<Loan> list = this._loanLogic.GetAllLoans().Where(x => x.Name.Contains(search) || x.ProductID.ToString() == search);
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var item in list)
            {
                this.GetAllItem.Add(item);
            }
        }

        /// <summary>
        /// Search the searched user.
        /// </summary>
        /// <param name="search"> The searched user. </param>
        private void SearchUser(string search)
        {
            IEnumerable<User> list = this._userLogic.GetAllUsers().Where(x => x.Name.Contains(search) || x.UserID.ToString() == search);
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var item in list)
            {
                this.GetAllItem.Add(item);
            }
        }

        /// <summary>
        /// Search the searched customer.
        /// </summary>
        /// <param name="search"> The searched customer. </param>
        private void SearchCustomer(string search)
        {
            IEnumerable<Customer> list = this._customerLogic.GetAllCustomers().Where(x => x.Name.Contains(search) || x.CustomerID.ToString() == search);
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var item in list)
            {
                this.GetAllItem.Add(item);
            }
        }

        /// <summary>
        /// Get all contract to the GetAllItem list.
        /// </summary>
        private void CreateContractList()
        {
            IEnumerable<Contract> list = this._contractLogic.GetAllContracts();
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var contract in list)
            {
                this.GetAllItem.Add(contract);
            }
        }

        /// <summary>
        /// Get all customer to the GetAllItem list.
        /// </summary>
        private void CreateCustomerList()
        {
            IEnumerable<Customer> list = this._customerLogic.GetAllCustomers();
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var customer in list)
            {
                this.GetAllItem.Add(customer);
            }
        }

        /// <summary>
        /// Get all loan to the GetAllItem list.
        /// </summary>
        private void CretaeLoanList()
        {
            IEnumerable<Loan> list = this._loanLogic.GetAllLoans();
            this.GetAllItem = new ObservableCollection<object>();
            foreach (var loan in list)
            {
                this.GetAllItem.Add(loan);
            }
        }
    }
}
