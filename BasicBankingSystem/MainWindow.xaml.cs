﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using DatabaseClassLibrary;
    using LogicClassLibrary;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this._vm = new MainWindowViewModel(new BbsUserLogic(new BbsRepo()));
        }

        /// <summary>
        /// Start the login when click on login button.
        /// </summary>
        /// <param name="sender"> The sender object. </param>
        /// <param name="e"> The created RoutedEventArgs. </param>
        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            this.Login();
        }

        /// <summary>
        /// Start the login when press enter.
        /// </summary>
        /// <param name="sender"> The sender object. </param>
        /// <param name="e"> The created RoutedEventArgs. </param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                this.Login();
            }
        }

        /// <summary>
        /// Login with given username and password.
        /// </summary>
        private void Login()
        {
            this.Cursor = Cursors.Wait;
            try
            {
                int.Parse(this.userIDText.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("The User ID is wrong format", "Warning", MessageBoxButton.OK);
                this.Cursor = Cursors.Arrow;
                return;
            }

            if (this.userIDText.Text == string.Empty)
            {
                MessageBox.Show("The User ID is empty", "Warning", MessageBoxButton.OK);
                this.Cursor = Cursors.Arrow;
                return;
            }

            if (this.userPassword.Password == string.Empty)
            {
                MessageBox.Show("The password is empty", "Warning", MessageBoxButton.OK);
                this.Cursor = Cursors.Arrow;
                return;
            }

            if (this._vm.CanLogIn(this.userIDText.Text, this.userPassword.Password))
            {
                new Menu(this._vm.IsBoss(this.userIDText.Text), int.Parse(this.userIDText.Text)).Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong username/password, please try again", "Warning", MessageBoxButton.OK);
            }

            this.Cursor = Cursors.Arrow;
        }
    }
}
