﻿// <copyright file="IsBossToCheckBoxIsCheckedConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BasicBankingSystem
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converter class to CheckBox.
    /// </summary>
    public class IsBossToCheckBoxIsCheckedConverter : IValueConverter
    {
        /// <summary>
        /// Converts the value of isBoss.
        /// </summary>
        /// <param name="value"> Value to convert. </param>
        /// <param name="targetType"> Target type not used. </param>
        /// <param name="parameter"> Parameter not used. </param>
        /// <param name="culture"> Culture not used. </param>
        /// <returns> Retruns true if isboss is Y. </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string isBoss = (string)value;
            return isBoss == "Y";
        }

        /// <summary>
        /// ConvertBack is not implemented
        /// </summary>
        /// <param name="value"> Not used. </param>
        /// <param name="targetType"> Targettype not used. </param>
        /// <param name="parameter"> Parameter not used. </param>
        /// <param name="culture"> Culture not used. </param>
        /// <returns> Throws not implemented excepiton. </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "Y" : "N";
        }
    }
}
