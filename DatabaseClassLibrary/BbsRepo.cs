﻿// <copyright file="BbsRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DatabaseClassLibrary
{
    using System.Linq;

    /// <summary>
    /// Repository class.
    /// </summary>
    public class BbsRepo : IBbsRepo
    {
        private BasicBankingSystemDatabaseEntities _dB;

        /// <summary>
        /// Initializes a new instance of the <see cref="BbsRepo"/> class.
        /// </summary>
        public BbsRepo()
        {
            this._dB = new BasicBankingSystemDatabaseEntities();
        }

        /// <summary>
        /// Add contract to the Contract table.
        /// </summary>
        /// <param name="contract"> The used contract. </param>
        public void AddContractToDatabase(Contract contract)
        {
            this._dB.Contracts.Add(contract);
            this._dB.SaveChanges();
        }

        /// <summary>
        /// Add customer to the Customer table.
        /// </summary>
        /// <param name="customer"> The used customer. </param>
        public void AddCustomerToDatabase(Customer customer)
        {
            this._dB.Customers.Add(customer);
            this._dB.SaveChanges();
        }

        /// <summary>
        /// Add loan to the Loan table.
        /// </summary>
        /// <param name="loan"> The used contract. </param>
        public void AddLoanToDatabase(Loan loan)
        {
            this._dB.Loans.Add(loan);
            this._dB.SaveChanges();
        }

        /// <summary>
        /// Add user to the User table.
        /// </summary>
        /// <param name="user"> The used user. </param>
        public void AddUserToDatabase(User user)
        {
            this._dB.Users.Add(user);
            this._dB.SaveChanges();
        }

        /// <summary>
        /// Get all contract from the Contract table.
        /// </summary>
        /// <returns> One contract collection. </returns>
        public IQueryable<Contract> GetAllContract()
        {
            this._dB = new BasicBankingSystemDatabaseEntities();

            return this._dB.Contracts;
        }

        /// <summary>
        /// Get all customer from the Customer table.
        /// </summary>
        /// <returns> One customer collection. </returns>
        public IQueryable<Customer> GetAllCustomer()
        {
            this._dB = new BasicBankingSystemDatabaseEntities();

            return this._dB.Customers;
        }

        /// <summary>
        /// Get all loan from the Loan table.
        /// </summary>
        /// <returns> One loan collection. </returns>
        public IQueryable<Loan> GetAllLoan()
        {
            return this._dB.Loans;
        }

        /// <summary>
        /// Get all user from the User table.
        /// </summary>
        /// <returns> One user collection. </returns>
        public IQueryable<User> GetAllUser()
        {
            return this._dB.Users;
        }

        /// <summary>
        /// Update the database.
        /// </summary>
        public void UpdateDatabase()
        {
            this._dB.SaveChanges();
        }
    }
}
