﻿// <copyright file="IBbsRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DatabaseClassLibrary
{
    using System.Linq;

    /// <summary>
    /// Interface of the repository.
    /// </summary>
    public interface IBbsRepo
    {
        // Create

        /// <summary>
        /// Add user to the User table.
        /// </summary>
        /// <param name="user"> The used user. </param>
        void AddUserToDatabase(User user);

        /// <summary>
        /// Add customer to the Customer table.
        /// </summary>
        /// <param name="customer"> The used customer. </param>
        void AddCustomerToDatabase(Customer customer);

        /// <summary>
        /// Add loan to the Loan table.
        /// </summary>
        /// <param name="loan"> The used contract. </param>
        void AddLoanToDatabase(Loan loan);

        /// <summary>
        /// Add contract to the Contract table.
        /// </summary>
        /// <param name="contract"> The used contract. </param>
        void AddContractToDatabase(Contract contract);

        // Read

        /// <summary>
        /// Get all user from the User table.
        /// </summary>
        /// <returns> One user collection. </returns>
        IQueryable<User> GetAllUser();

        /// <summary>
        /// Get all loan from the Loan table.
        /// </summary>
        /// <returns> One loan collection. </returns>
        IQueryable<Loan> GetAllLoan();

        /// <summary>
        /// Get all contract from the Contract table.
        /// </summary>
        /// <returns> One contract collection. </returns>
        IQueryable<Contract> GetAllContract();

        /// <summary>
        /// Get all customer from the Customer table.
        /// </summary>
        /// <returns> One customer collection. </returns>
        IQueryable<Customer> GetAllCustomer();

        // Update

        /// <summary>
        /// Update the database.
        /// </summary>
        void UpdateDatabase();
    }
}
