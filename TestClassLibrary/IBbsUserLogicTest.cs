﻿// <copyright file="IBbsUserLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;
    using LogicClassLibrary;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class of the IBbsUserLogic.
    /// </summary>
    [TestFixture]
    public class IBbsUserLogicTest
    {
        private MockGenerator mockGenerator;

        /// <summary>
        /// Declare new MockGenerator.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockGenerator = new MockGenerator();
        }

        /// <summary>
        /// Test the CreateUser method with incorrect datas.
        /// </summary>
        /// <param name="password"> The password of the user. </param>
        /// <param name="name"> The name of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="hiredate"> The hiredate of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The address of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        [TestCase(null, "goodpassword1", "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("", "goodpassword1", "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Too long nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "goodpassword1", "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", null, "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "", "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "To long passwoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooord", "100000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", null, "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "1g", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "2000000000", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "-1", "2018.04.13.", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", null, "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "20g", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", null, "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+3670939", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+3630939578888", "Administrator Street 8.", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+36309395788", null, "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+36309395788", "", "N")]
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+36309395788", "To long addreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeess", "N")]
        public void WhenCreateUserWithIncorrectData_ThenReturnNotNullAndNotAddNewRowToTheUsersTable(string password, string name, string salary, string hiredate, string phone, string address, string isboss)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateUserResult = testUserLogic.CreateUser(password, name, hiredate, salary, phone, address, isboss);

            // ASSERT
            Assert.That(testCreateUserResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddUserToDatabase(It.IsAny<User>()), Times.Never);
        }

        /// <summary>
        /// Test the CreateUser method with correct datas.
        /// </summary>
        /// <param name="password"> The password of the user. </param>
        /// <param name="name"> The name of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="hiredate"> The hiredate of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The address of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        [TestCase("Test Administrator", "goodpassword1", "100000", "2018.04.13.", "+36309395788", "Administrator Street 8.", "N")]
        public void WhenCreateUserWithCorrectDatas_ThenReturnNullAndAddNewRowToTheUsersTable(string password, string name, string salary, string hiredate, string phone, string address, string isboss)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateUserResult = testUserLogic.CreateUser(password, name, hiredate, salary, phone, address, isboss);

            // ASSERT
            Assert.That(testCreateUserResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddUserToDatabase(It.IsAny<User>()), Times.Once);
        }

        /// <summary>
        /// Test GetAllUser method.
        /// </summary>
        [Test]
        public void WhenGetAllUser_ThenUserListIsEqualToTestUserList()
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<User> testGetAllUserResult = testUserLogic.GetAllUsers().ToList();

            // ASSERT
            Assert.That(testGetAllUserResult, Is.EqualTo(this.mockGenerator.TestUserList));
        }

        /// <summary>
        /// Test GetUserByUserID method with incorrect user id.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        [TestCase(-1)]
        public void WhenGetUserByUserIDWithIncorrectUserID_ThenUserIsNull(int userid)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            User testGetUserByUserIDResult = testUserLogic.GetUserByID(userid);

            // ASSERT
            Assert.That(testGetUserByUserIDResult, Is.Null);
        }

        /// <summary>
        /// Test GetUserByUserID method with correct user id.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        [TestCase(1)]
        public void WhenGetUserByUserIDWithCorrectUserID_ThenUserIsEqualToTestUser(int userid)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            User testGetUserByUserIDResult = testUserLogic.GetUserByID(userid);

            // ASSERT
            Assert.That(testGetUserByUserIDResult, Is.EqualTo(this.mockGenerator.TestUserList.Single(test => test.UserID == userid)));
        }

        /// <summary>
        /// Test GetUserByName method with incorrect name.
        /// </summary>
        /// <param name="name"> The name of the user. </param>
        [TestCase(null)]
        public void WhenGetUserByUserNameWithIncorrectUserName_ThenThrowNullReferenceException(string name)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT, ASSERT
            Assert.That(() => testUserLogic.GetUserByName(name).ToList(), Throws.TypeOf<NullReferenceException>());
        }

        /// <summary>
        /// Test GetUserByName method with correct name.
        /// </summary>
        /// <param name="name"> The name of the user. </param>
        [TestCase("Admin")]
        public void WhenGetUserByUserNameWithCorrectUserName_ThenAllItemInUserListEqualToTestUserList(string name)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<User> testGetUserByUserNameResult = testUserLogic.GetUserByName(name).ToList();

            // ASSERT
            Assert.That(testGetUserByUserNameResult, Is.EqualTo(this.mockGenerator.TestUserList.Where(test => test.Name.ToLower().Contains(name.ToLower()))));
        }

        /// <summary>
        /// Test the LoginUser method with incorrect user id.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        [TestCase(-1, "bosspw")]
        public void WhenLoginUserWithIncorrectUserID_ThenTestResultIsEqualToMinusOne(int userid, string password)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            int testLoginUserResult = testUserLogic.LoginUser(userid, password);

            // ASSERT
            Assert.That(testLoginUserResult, Is.EqualTo(-1));
        }

        /// <summary>
        /// Test the LoginUser method with incorrect password.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        [TestCase(1, "")]
        [TestCase(1, null)]
        public void WhenLoginUserWithIncorrectPassword_ThenTestResultIsEqualToNull(int userid, string password)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            int testLoginUserResult = testUserLogic.LoginUser(userid, password);

            // ASSERT
            Assert.That(testLoginUserResult, Is.EqualTo(0));
        }

        /// <summary>
        /// Test the LoginUser method with not match datas.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        [TestCase(1, "bpw")]
        public void WhenLoginUserWithNotMatchPasswordWithUserID_ThenTestResultIsEqualToNull(int userid, string password)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            int testLoginUserResult = testUserLogic.LoginUser(userid, password);

            // ASSERT
            Assert.That(testLoginUserResult, Is.EqualTo(0));
        }

        /// <summary>
        /// Test the LoginUser method with match datas.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        [TestCase(1, "bosspw")]
        public void WhenLoginUserWithMatchDatas_ThenTestResultIsEqualToOne(int userid, string password)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            int testLoginUserResult = testUserLogic.LoginUser(userid, password);

            // ASSERT
            Assert.That(testLoginUserResult, Is.EqualTo(1));
        }

        /// <summary>
        /// Test the UpdateUser method with incorrect datas.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        /// <param name="name"> The name of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The address of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        [TestCase(1, null, "goodpassword1", null, "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "", "goodpassword1", null, "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Too long nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "goodpassword1", "100000", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", null, "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "1g", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "2000000000", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "-1", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", null, "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "+3670939", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "+3630939578888", "Administrator Street 8.", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "+36309395788", null, "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "+36309395788", "", "N")]
        [TestCase(1, "Test Administrator", "goodpassword1", "100000", "+36309395788", "To long addreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeess", "N")]
        public void WhenUpdateUserWithIncorrectData_ThenReturnNotNullAndTheDatasNotUpdatingInTheUsersTable(int userid, string password, string name, string salary, string phone, string address, string isboss)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateUserResult = testUserLogic.UpdateUser(userid, password, name, salary, phone, address, isboss);

            // ASSERT
            Assert.That(testUpdateUserResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Never);
        }

        /// <summary>
        /// Test the UpdateUser method with correct datas.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        /// <param name="name"> The name of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The address of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        [TestCase(1, null, "Test User", "100000", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "", "Test User", "100000", "+36709395788", "Administrator Street 8.", "N")]
        [TestCase(1, "goodpassword1", "Test User", "100000", "+36709395788", "Administrator Street 8.", "N")]
        public void WhenUpdateUserWithCorrectDatas_ThenReturnNullAndTheDatasUpdatingInTheUsersTable(int userid, string password, string name, string salary, string phone, string address, string isboss)
        {
            // ARRANGE
            IBbsUserLogic testUserLogic = new BbsUserLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateUserResult = testUserLogic.UpdateUser(userid, name, password, salary, phone, address, isboss);

            // ASSERT
            Assert.That(testUpdateUserResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Once);
        }
    }
}
