﻿// <copyright file="MockGenerator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;
    using LogicClassLibrary;
    using Moq;

    /// <summary>
    /// This class generate fake mock repository and datas.
    /// </summary>
    public class MockGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MockGenerator"/> class.
        /// </summary>
        public MockGenerator()
        {
            this.IBbsRepoMock = new Mock<IBbsRepo>();

            this.TestContractList = new List<Contract>();
            this.TestCustomerList = new List<Customer>();
            this.TestLoanList = new List<Loan>();
            this.TestUserList = new List<User>();

            this.UploadTestCustomerListWithDatas();
            this.UploadTestLoanListWithDatas();
            this.UploadTestUserListWithDatas();
            this.UploadTestContractListWithDatas();

            this.SetupRepoToGetAll();
            this.SetupRepoToAdd();
            this.SetupRepoToUpdate();
        }

        /// <summary>
        /// Gets or sets the mock repository.
        /// </summary>
        public Mock<IBbsRepo> IBbsRepoMock { get; set; }

        /// <summary>
        /// Gets or sets the mock contract list.
        /// </summary>
        public List<Contract> TestContractList { get; set; }

        /// <summary>
        /// Gets or sets the mock customer list.
        /// </summary>
        public List<Customer> TestCustomerList { get; set; }

        /// <summary>
        /// Gets or sets the mock loan list.
        /// </summary>
        public List<Loan> TestLoanList { get; set; }

        /// <summary>
        /// Gets or sets the mock user list.
        /// </summary>
        public List<User> TestUserList { get; set; }

        /// <summary>
        /// Upload the TestContractList with datas.
        /// </summary>
        private void UploadTestContractListWithDatas()
        {
            this.TestContractList.Add(new Contract() { ContractID = 1, CustomerID = 3, ProductID = 1, UserID = 1, CreationDate = DateTime.Now, Amount = 4500000, Duration = 8, Name = 1 });
            this.TestContractList.Add(new Contract() { ContractID = 2, CustomerID = 3, ProductID = 2, UserID = 3, CreationDate = DateTime.Now, Amount = 2000000, Duration = 2, Name = 2 });
            this.TestContractList.Add(new Contract() { ContractID = 3, CustomerID = 1, ProductID = 2, UserID = 2, CreationDate = DateTime.Now, Amount = 800000, Duration = 1, Name = 3 });
            this.TestContractList.Add(new Contract() { ContractID = 4, CustomerID = 5, ProductID = 1, UserID = 2, CreationDate = DateTime.Now, Amount = 7000000, Duration = 10, Name = 4 });
        }

        /// <summary>
        /// Upload the TestCustomerList with datas.
        /// </summary>
        private void UploadTestCustomerListWithDatas()
        {
            this.TestCustomerList.Add(new Customer() { CustomerID = 1, Name = "Mock Sandor", Addr = "Budapest, Mock street 8.", BirthDate = new DateTime(1980, 06, 13), AccNum = "12345", Salary = 10000, CStatus = string.Empty });
            this.TestCustomerList.Add(new Customer() { CustomerID = 2, Name = "Test Benedek", Addr = "Hatvan, Test street 42.", BirthDate = new DateTime(1967, 07, 07), AccNum = "23456", Salary = 90000, CStatus = string.Empty });
            this.TestCustomerList.Add(new Customer() { CustomerID = 3, Name = "Test Programmer", Addr = "Dorog, Programmer street 1.", BirthDate = new DateTime(1997, 04, 13), AccNum = "34567", Salary = 780000, CStatus = string.Empty });
            this.TestCustomerList.Add(new Customer() { CustomerID = 4, Name = "Deleted Customer", Addr = "Budapest, Deleted street 9.", BirthDate = new DateTime(1990, 12, 30), AccNum = "45678", Salary = 350000, CStatus = "C" });
            this.TestCustomerList.Add(new Customer() { CustomerID = 5, Name = "Last Customer", Addr = "Esztergom, Last street 70.", BirthDate = new DateTime(1981, 02, 07), AccNum = "5678", Salary = 530000, CStatus = string.Empty });
        }

        /// <summary>
        /// Upload the TestLoanList with datas.
        /// </summary>
        private void UploadTestLoanListWithDatas()
        {
            this.TestLoanList.Add(new Loan() { ProductID = 1, Name = "Home loan", Descr = "This loan can be spend on house rebuilt or buy.", MaxAmount = 10000000, MaxDur = 10, MinSal = 250000, LoanAPR = 13 });
            this.TestLoanList.Add(new Loan() { ProductID = 2, Name = "Car loan", Descr = "This loan can be spend on car buy.", MaxAmount = 7500000, MaxDur = 4, MinSal = 80000, LoanAPR = 7 });
        }

        /// <summary>
        /// Upload the TestuserList with datas.
        /// </summary>
        private void UploadTestUserListWithDatas()
        {
            BbsUserLogic testUserLogic = new BbsUserLogic(this.IBbsRepoMock.Object);

            this.TestUserList.Add(new User() { UserID = 1, UserPW = testUserLogic.HashUserPW("bosspw"), Name = "Administrator Boss", HireDate = new DateTime(2001, 01, 01), Salary = 900000, Phone = "+36701111111", Addr = "Budapest, Boss street 12.", IsBoss = "Y" });
            this.TestUserList.Add(new User() { UserID = 2, UserPW = testUserLogic.HashUserPW("admino"), Name = "Administrator One", HireDate = new DateTime(2005, 09, 08), Salary = 350000, Phone = "+36204567777", Addr = "Budapest, Adminstrator street 18.", IsBoss = "N" });
            this.TestUserList.Add(new User() { UserID = 3, UserPW = testUserLogic.HashUserPW("admint"), Name = "Administrator Two", HireDate = new DateTime(2018, 04, 22), Salary = 80000, Phone = "+36301213443", Addr = "Budapest, Adminstrator street 19.", IsBoss = "N" });
        }

        /// <summary>
        /// Setup the IBbsRepoMock to GetAll methods.
        /// </summary>
        private void SetupRepoToGetAll()
        {
            this.IBbsRepoMock.Setup(test => test.GetAllContract()).Returns(this.TestContractList.AsQueryable);
            this.IBbsRepoMock.Setup(test => test.GetAllCustomer()).Returns(this.TestCustomerList.AsQueryable);
            this.IBbsRepoMock.Setup(test => test.GetAllLoan()).Returns(this.TestLoanList.AsQueryable);
            this.IBbsRepoMock.Setup(test => test.GetAllUser()).Returns(this.TestUserList.AsQueryable);
        }

        /// <summary>
        /// Setup the IBbsRepoMock to Add methods.
        /// </summary>
        private void SetupRepoToAdd()
        {
            this.IBbsRepoMock.Setup(test => test.AddContractToDatabase(It.IsAny<Contract>()));
            this.IBbsRepoMock.Setup(test => test.AddCustomerToDatabase(It.IsAny<Customer>()));
            this.IBbsRepoMock.Setup(test => test.AddLoanToDatabase(It.IsAny<Loan>()));
            this.IBbsRepoMock.Setup(test => test.AddUserToDatabase(It.IsAny<User>()));
        }

        /// <summary>
        /// Setup the IBbsRepoMock to update method.
        /// </summary>
        private void SetupRepoToUpdate()
        {
            this.IBbsRepoMock.Setup(test => test.UpdateDatabase());
        }
    }
}
