﻿// <copyright file="IBbsCustomerLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;
    using LogicClassLibrary;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class of the IBbsCustomerLogic.
    /// </summary>
    [TestFixture]
    public class IBbsCustomerLogicTest
    {
        private MockGenerator mockGenerator;

        /// <summary>
        /// Declare new MockGenerator.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockGenerator = new MockGenerator();
        }

        /// <summary>
        /// Test the CreateCustomer method with incorrect datas.
        /// </summary>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        [TestCase(null, "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase("", "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase("To long nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase("New Customer", null, "2018.04.13.", "11111", "500000")]
        [TestCase("New Customer", "", "2018.04.13.", "11111", "500000")]
        [TestCase("New Customer", "To long addreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeess", "2018.04.13.", "11111", "500000")]
        [TestCase("New Customer", "Customer Street 12.", null, "", "500000")]
        [TestCase("New Customer", "Customer Street 12.", "", "", "500000")]
        [TestCase("New Customer", "Customer Street 12.", "20g", "", "500000")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", null, "500000")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "", "500000")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111111111111111111111111111", "500000")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111", null)]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111", "")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111", "2000000000")]
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111", "-1")]
        public void WhenCreateCustomerWithIncorrectData_ThenReturnNotNullAndNotAddNewRowToTheCustomersTable(string name, string address, string birthday, string accnum, string salary)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateCustomerResult = testCustomerLogic.CreateCustomer(name, address, birthday, accnum, salary);

            // ASSERT
            Assert.That(testCreateCustomerResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddCustomerToDatabase(It.IsAny<Customer>()), Times.Never);
        }

        /// <summary>
        /// Test the CreateCustomer method with correct data.
        /// </summary>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        [TestCase("New Customer", "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        public void WhenCreateCustomerWithCorrectDatas_ThenReturnNullAndAddNewRowToTheCustomersTable(string name, string address, string birthday, string accnum, string salary)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateCustomerResult = testCustomerLogic.CreateCustomer(name, address, birthday, accnum, salary);

            // ASSERT
            Assert.That(testCreateCustomerResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddCustomerToDatabase(It.IsAny<Customer>()), Times.Once);
        }

        /// <summary>
        /// Test the GetAllCustomers method.
        /// </summary>
        [Test]
        public void WhenGetAllCustomers_ThenCustomerListIsEqualToTestCustomerList()
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Customer> testGetAllCustomerResult = testCustomerLogic.GetAllCustomers().ToList();

            // ASSERT
            Assert.That(testGetAllCustomerResult, Is.EqualTo(this.mockGenerator.TestCustomerList.Where(test => test.CStatus != "C")));
        }

        /// <summary>
        /// Test the GetCustomerByCustomerID method with incorrect customer ID.
        /// </summary>
        /// <param name="customerid"> The ID of the customer. </param>
        [TestCase(-1)]
        public void WhenGetCustomerByCustomerIDWithIncorrectCustomerID_ThenReturnNull(int customerid)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Customer testGetCustomerByCustomerIDResult = testCustomerLogic.GetCustByID(customerid);

            // ASSERT
            Assert.That(testGetCustomerByCustomerIDResult, Is.Null);
        }

        /// <summary>
        /// Test the GetCustomerByCustomerID method with correct customer ID.
        /// </summary>
        /// <param name="customerid"> The ID of the customer. </param>
        [TestCase(1)]
        public void WhenGetCustomerByCustomerIDWithCorrectCustomerID_ThenCustomerIsEqualToTestCustomer(int customerid)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Customer testGetCustomerByCustomerIDResult = testCustomerLogic.GetCustByID(customerid);

            // ASSERT
            Assert.That(testGetCustomerByCustomerIDResult, Is.EqualTo(this.mockGenerator.TestCustomerList.Single(test => test.CustomerID == customerid)));
        }

        /// <summary>
        /// Test the GetCustomersByCustomerName method with incorrect name.
        /// </summary>
        /// <param name="customername"> The name of the customer. </param>
        [TestCase(null)]
        public void WhenGetCustomersByCustomerNameWithIncorrectCustomerName_ThenThrowNullReferenceException(string customername)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT, ASSERT
            Assert.That(() => testCustomerLogic.GetCustByName(customername).ToList(), Throws.TypeOf<NullReferenceException>());
        }

        /// <summary>
        /// Test the GetCustomersByCustomerName method with correct name.
        /// </summary>
        /// <param name="customername"> The name of the customer. </param>
        [TestCase("Customer")]
        public void WhenGetCustomersByCustomerNameWithCorrectCustomerName_ThenAllItemInCustomerListEqualToTestCustomerList(string customername)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Customer> testGetCustomerByCustomerNameResult = testCustomerLogic.GetCustByName(customername).ToList();

            // ASSERT
            Assert.That(testGetCustomerByCustomerNameResult, Is.EqualTo(this.mockGenerator.TestCustomerList.Where(test => test.Name.ToLower().Contains(customername.ToLower()) && test.CStatus != "C")));
        }

        /// <summary>
        /// Test the UpdateCustomer method with incorrect datas.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        [TestCase(1, null, "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase(1, "", "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase(1, "To long nameeeeeeeeeeeeeeeeeeee", "Customer Street 12.", "2018.04.13.", "11111", "500000")]
        [TestCase(1, "New Customer", null, "2018.04.13.", "11111", "500000")]
        [TestCase(1, "New Customer", "", "2018.04.13.", "11111", "500000")]
        [TestCase(1, "New Customer", "To long addreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeess", "2018.04.13.", "11111", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", null, "", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "", "", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "20g", "", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", null, "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "11111111111111111111111111111", "500000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "11111", null)]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "11111", "")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "11111", "2000000000")]
        [TestCase(1, "New Customer", "Customer Street 12.", "2018.04.13.", "11111", "-1")]
        public void WhenUpdateCustomerWithIncorrectData_ThenReturnNotNullAndTheDatasNotUpdatingInTheCustomersTable(int custid, string name, string birthday, string address, string accnum, string salary)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateCustomerResult = testCustomerLogic.UpdateCustomer(custid, name, address, birthday, accnum, salary);

            // ASSERT
            Assert.That(testUpdateCustomerResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Never);
        }

        /// <summary>
        /// Test the UpdateCustomer method with correct data.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        [TestCase(1, "New Customer", "2018.04.13.", "Customer Street 12.", "11111", "500000")]
        public void WhenUpdateCustomerWithCorrectData_ThenReturnNullAndTheDatasUpdatingInTheCustomersTable(int custid, string name, string birthday, string address, string accnum, string salary)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateCustomerResult = testCustomerLogic.UpdateCustomer(custid, name, address, birthday, accnum, salary);

            // ASSERT
            Assert.That(testUpdateCustomerResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Once);
        }

        /// <summary>
        /// Test DeleteCustomerme method with incorrect customer ID.
        /// </summary>
        /// <param name="customerid"> The ID of the customer. </param>
        [TestCase(-1)]
        public void WhenDeleteCustomerWithIncorrectCustomerID_ThenReturnFalseAndNotDeleteTheCustomerFromTheCustomerTable(int customerid)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            bool testDeleteCustomerResult = testCustomerLogic.DeleteCustomer(customerid);

            // ASSERT
            Assert.That(testDeleteCustomerResult, Is.EqualTo(false));
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Never);
        }

        /// <summary>
        /// Test DeleteCustomer method with correct customer ID.
        /// </summary>
        /// <param name="customerid"> The ID of the customer. </param>
        [TestCase(1)]
        public void WhenDeleteCustomerWithCorrectCustomerID_ThenReturnTrueAndDeleteTheCustomerFromTheCustomerTable(int customerid)
        {
            // ARRANGE
            IBbsCustomerLogic testCustomerLogic = new BbsCustomerLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            bool testDeleteCustomerResult = testCustomerLogic.DeleteCustomer(customerid);

            // ASSERT
            Assert.That(testDeleteCustomerResult, Is.EqualTo(true));
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Once);
        }
    }
}
