﻿// <copyright file="IBbsLoanLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;
    using LogicClassLibrary;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class of the IBbsLoanLogic.
    /// </summary>
    [TestFixture]
    public class IBbsLoanLogicTest
    {
        private MockGenerator mockGenerator;

        /// <summary>
        /// Declare new MockGenerator.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockGenerator = new MockGenerator();
        }

        /// <summary>
        /// Test the CreateLoan method with incorrect datas.
        /// </summary>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="desc"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The maximum duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the user. </param>
        /// <param name="loanapr"> The APR of the loan. </param>
        [TestCase(null, "New loan description", "100000", " 1", "50000", "4")]
        [TestCase("", "New loan description", "100000", "1", "50000", "4")]
        [TestCase("Too long nameeeeeeeeeeeee", "New loan description", "100000", "1", "50000", "4")]
        [TestCase("Test Loan", null, "100000", "1", "50000", "4")]
        [TestCase("Test Loan", "", "100000", "1", "50000", "4")]
        [TestCase("Test Loan", "Too long descriptiooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooon", "100000", "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", null, "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "", "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "1g", "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "10000000000", "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "-1", "1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", null, "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1g", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "10000000000", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "-1", "50000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", null, "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "1g", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "100000000", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "-1", "4")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "50000", null)]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "50000", "")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "50000", "1g")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "50000", "101")]
        [TestCase("Test Loan", "New Loan description", "100000", "1", "50000", "-1")]
        public void WhenCreateLoanWithIncorrectData_ThenReturnNotNullAndNotAddNewRowToTheLoansTable(string name, string desc, string maxamount, string maxdur, string minsal, string loanapr)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateLoanResult = testLoanLogic.CreateLoan(name, desc, maxamount, maxdur, minsal, loanapr);

            // ASSERT
            Assert.That(testCreateLoanResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddLoanToDatabase(It.IsAny<Loan>()), Times.Never);
        }

        /// <summary>
        /// Test the CreateLoan method with correct data.
        /// </summary>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="desc"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The maximum duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the user. </param>
        /// <param name="loanapr"> The annual percentage rate of the loan. </param>
        [TestCase("Test Loan", "New loan description", "100000", "1", "50000", "4")]
        public void WhenCreateLoanWithCorrectDatas_ThenReturnNullAndAddNewRowToTheLoansTable(string name, string desc, string maxamount, string maxdur, string minsal, string loanapr)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateLoanResult = testLoanLogic.CreateLoan(name, desc, maxamount, maxdur, minsal, loanapr);

            // ASSERT
            Assert.That(testCreateLoanResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddLoanToDatabase(It.IsAny<Loan>()), Times.Once);
        }

        /// <summary>
        /// Test the GetAllLoan method.
        /// </summary>
        [Test]
        public void WhenGetAllLoan_ThenLoanListIsEqualToTestLoanList()
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Loan> testGetAllLoanResult = testLoanLogic.GetAllLoans().ToList();

            // ASSERT
            Assert.That(testGetAllLoanResult, Is.EqualTo(this.mockGenerator.TestLoanList));
        }

        /// <summary>
        /// Test the GetLoanByLoanID method with incorrect loan ID.
        /// </summary>
        /// <param name="productid"> The ID of the product. </param>
        [TestCase(-1)]
        public void WhenGetLoanByLoanIDWithIncorrectLoanID_ThenReturnNull(int productid)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Loan testGetLoanByLoanIDResult = testLoanLogic.GetLoanByID(productid);

            // ASSERT
            Assert.That(testGetLoanByLoanIDResult, Is.Null);
        }

        /// <summary>
        /// Test the GetLoanByLoanID method with correct loan ID.
        /// </summary>
        /// <param name="productid"> The ID of the product. </param>
        [TestCase(1)]
        public void WhenGetLoanByLoanIDWithCorrectLoanID_ThenLoanIDIsEqualToTestLoanID(int productid)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Loan testGetLoanByLoanIDResult = testLoanLogic.GetLoanByID(productid);

            // ASSERT
            Assert.That(testGetLoanByLoanIDResult.ProductID, Is.EqualTo(productid));
        }

        /// <summary>
        /// Test the GetLoansByProductName method with incorrect product name.
        /// </summary>
        /// <param name="prodname"> The name of the product. </param>
        [TestCase(null)]
        public void WhenGetLoansByProductNameWithInvalidProductName_ThenThrowNullReferenceException(string prodname)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT, ASSERT
            Assert.That(() => testLoanLogic.GetLoanByName(prodname).ToList(), Throws.TypeOf<NullReferenceException>());
        }

        /// <summary>
        /// Test the GetLoansByProductName method with correct product name.
        /// </summary>
        /// <param name="prodname"> The name of the product. </param>
        [TestCase("Loan")]
        public void WhenGetLoansByProductNameWithCorrectProductName_ThenAllItemInLoanListEqualToTestLoanList(string prodname)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Loan> testGetLoanByProductNameResult = testLoanLogic.GetLoanByName(prodname).ToList();

            // ASSERT
            Assert.That(testGetLoanByProductNameResult, Is.EqualTo(this.mockGenerator.TestLoanList.Where(test => test.Name.ToLower().Contains(prodname.ToLower()))));
        }

        /// <summary>
        /// Test the UpdateLoan method with incorrect datas.
        /// </summary>
        /// <param name="productid"> The ID of the product. </param>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="descr"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The max duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the loan. </param>
        /// <param name="loanapr"> The annual percentage rate of the loan. </param>
        [TestCase(1, null, "New loan description", "100000", " 1", "50000", "4")]
        [TestCase(1, "", "New loan description", "100000", "1", "50000", "4")]
        [TestCase(1, "Too long nameeeeeeeeeeeee", "New loan description", "100000", "1", "50000", "4")]
        [TestCase(1, "Test Loan", null, "100000", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "", "100000", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "Too long descriptiooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooon", "100000", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", null, "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "1g", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "10000000000", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "-1", "1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", null, "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1g", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "10000000000", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "-1", "50000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", null, "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "1g", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "100000000", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "-1", "4")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", null)]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", "")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", "1g")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", "101")]
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", "-1")]
        public void WhenUpdateLoanWithIncorrectData_ThenReturnNotNullAndTheDatasNotUpdatingInTheLoansTable(int productid, string name, string descr, string maxamount, string maxdur, string minsal, string loanapr)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateLoanResult = testLoanLogic.UpdateLoan(productid, name, descr, maxamount, maxdur, minsal, loanapr);

            // ASSERT
            Assert.That(testUpdateLoanResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Never);
        }

        /// <summary>
        /// Test the UpdateLoan method with correct data.
        /// </summary>
        /// <param name="productid"> The ID of the product. </param>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="descr"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The max duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the loan. </param>
        /// <param name="loanapr"> The annual percentage rate of the loan. </param>
        [TestCase(1, "Test Loan", "New Loan description", "100000", "1", "50000", "4")]
        public void WhenUpdateLoanWithCorrectDatas_ThenReturnNullAndTheDatasUpdatingInTheLoansTable(int productid, string name, string descr, string maxamount, string maxdur, string minsal, string loanapr)
        {
            // ARRANGE
            IBbsLoanLogic testLoanLogic = new BbsLoanLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testUpdateLoanResult = testLoanLogic.UpdateLoan(productid, name, descr, maxamount, maxdur, minsal, loanapr);

            // ASSERT
            Assert.That(testUpdateLoanResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.UpdateDatabase(), Times.Once);
        }
    }
}
