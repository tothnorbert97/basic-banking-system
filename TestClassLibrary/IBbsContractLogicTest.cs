﻿// <copyright file="IBbsContractLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestClassLibrary
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;
    using LogicClassLibrary;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class of the IBbsContractLogic.
    /// </summary>
    [TestFixture]
    public class IBbsContractLogicTest
    {
        private MockGenerator mockGenerator;

        /// <summary>
        /// Declare new MockGenerator.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockGenerator = new MockGenerator();
        }

        /// <summary>
        /// Test the CreateContract method with incorrect datas.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <param name="productid"> The ID of the loan. </param>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="amount"> The amount of the contract. </param>
        /// <param name="duration"> The duration of the contract. </param>
        [TestCase(null, "2", 1, "100000", "1")]
        [TestCase("", "2", 1, "100000", "1")]
        [TestCase("1g", "2", 1, "100000", "1")]
        [TestCase("100", "2", 1, "100000", "1")]
        [TestCase("-1", "2", 1, "100000", "1")]
        [TestCase("1", null, 1, "100000", "1")]
        [TestCase("1", "", 1, "100000", "1")]
        [TestCase("1", "1g", 1, "100000", "1")]
        [TestCase("1", "100", 1, "100000", "1")]
        [TestCase("1", "-1", 1, "100000", "1")]
        [TestCase("1", "2", 1, null, "1")]
        [TestCase("1", "2", 1, "", "1")]
        [TestCase("1", "2", 1, "1g", "1")]
        [TestCase("1", "2", 1, "12000000000", "1")]
        [TestCase("1", "2", 1, "-1", "1")]
        [TestCase("1", "2", 1, "9000000000", "1")]
        [TestCase("1", "2", 1, "100000", null)]
        [TestCase("1", "2", 1, "100000", "")]
        [TestCase("1", "2", 1, "100000", "1g")]
        [TestCase("1", "2", 1, "100000", "1000")]
        [TestCase("1", "2", 1, "100000", "-1")]
        [TestCase("1", "2", 1, "100000", "600")]
        [TestCase("1", "2", 1, "100000", "2")]
        public void WhenCreateContractWithIncorrectData_ThenReturnNotNullAndNotAddNewRowToTheContractsTable(string custid, string productid, int userid, string amount, string duration)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateContractResult = testContractLogic.CreateContract(custid, productid, userid, amount, duration);

            // ASSERT
            Assert.That(testCreateContractResult, Is.Not.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddContractToDatabase(It.IsAny<Contract>()), Times.Never);
        }

        /// <summary>
        /// Test the CreateContract method with valid data.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <param name="productid"> The ID of the loan. </param>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="amount"> The amount of the contract. </param>
        /// <param name="duration"> The duration of the contract. </param>
        [TestCase("2", "2", 1, "10000", "1")]
        public void WhenCreateContractWithCorrectDatas_ThenReturnNullAndAddNewRowToTheContractsTable(string custid, string productid, int userid, string amount, string duration)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            string testCreateContractResult = testContractLogic.CreateContract(custid, productid, userid, amount, duration);

            // ASSERT
            Assert.That(testCreateContractResult, Is.Null);
            this.mockGenerator.IBbsRepoMock.Verify(test => test.AddContractToDatabase(It.IsAny<Contract>()), Times.Once);
        }

        /// <summary>
        /// Test the GetAllContract method.
        /// </summary>
        [Test]
        public void WhenGetAllContract_ThenContractListIsEqualToTestContractList()
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetAllContractResult = testContractLogic.GetAllContracts().ToList();

            // ASSERT
            Assert.That(testGetAllContractResult, Is.EqualTo(this.mockGenerator.TestContractList));
        }

        /// <summary>
        /// Test the GetContractsByCustomerID method with incorrect customer ID.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        [TestCase(-1)]
        public void WhenGetContractsByCustomerIDWithIncorrectCustomerID_ThenContractListCountEqualsNull(int custid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByCustomerIDResult = testContractLogic.GetContractsByCustomer(custid).ToList();

            // ASSERT
            Assert.That(testGetContractByCustomerIDResult.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Test the GetContractsByCustomerID method with correct customer ID.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        [TestCase(1)]
        public void WhenGetContractsByCustomerIDWithCorrectCustomerID_ThenContractListCountGreaterThanNull(int custid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByCustomerIDResult = testContractLogic.GetContractsByCustomer(custid).ToList();

            // ASSERT
            Assert.That(testGetContractByCustomerIDResult.Count, Is.GreaterThan(0));
        }

        /// <summary>
        /// Test the GetContractByProductID method with incorrect data.
        /// </summary>
        /// <param name="productid"> The ID of the loan. </param>
        [TestCase(-1)]
        public void WhenGetContractsByProductIDWithIncorrectProductID_ThenContractListCountEqualsNull(int productid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByProductIDResult = testContractLogic.GetContractsByProduct(productid).ToList();

            // ASSERT
            Assert.That(testGetContractByProductIDResult.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Test the GetContractByProductID method with correct data.
        /// </summary>
        /// <param name="productid"> The ID of the loan. </param>
        [TestCase(1)]
        public void WhenGetContractsByProductIDWithCorrectProductID_ThenContractListCountGreaterThanNull(int productid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByProductIDResult = testContractLogic.GetContractsByProduct(productid).ToList();

            // ASSERT
            Assert.That(testGetContractByProductIDResult.Count, Is.GreaterThan(0));
        }

        /// <summary>
        /// Test the GetContractsByUserID method with incorrect data.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        [TestCase(-1)]
        public void WhenGetContractsByUserIDWithIncorrectUserID_ThenContractListCountEqualsNull(int userid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByUserIDResult = testContractLogic.GetContractsByUser(userid).ToList();

            // ASSERT
            Assert.That(testGetContractByUserIDResult.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Test the GetContractsByUserID method with correct data.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        [TestCase(1)]
        public void WhenGetContractsByUserIDWithCorrectUserID_ThenContractListCountGreaterThanNull(int userid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            List<Contract> testGetContractByUserIDResult = testContractLogic.GetContractsByUser(userid).ToList();

            // ASSERT
            Assert.That(testGetContractByUserIDResult.Count, Is.GreaterThan(0));
        }

        /// <summary>
        /// Test the GetContractByContractID method with incorrect data.
        /// </summary>
        /// <param name="contractid"> The ID of the contract. </param>
        [TestCase(-1)]
        public void WhenGetContractByContractIDWithIncorrectContractID_ThenContractIsNull(int contractid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Contract testGetContractByContractIDResult = testContractLogic.GetContractByID(contractid);

            // ASSERT
            Assert.That(testGetContractByContractIDResult, Is.Null);
        }

        /// <summary>
        /// Test the GetContractByContractID method with correct data.
        /// </summary>
        /// <param name="contractid"> The ID of the contract. </param>
        [TestCase(1)]
        public void WhenGetContractByContractIDWithCorrectContractID_ThenContractIsEqualToTestContract(int contractid)
        {
            // ARRANGE
            IBbsContractLogic testContractLogic = new BbsContractLogic(this.mockGenerator.IBbsRepoMock.Object);

            // ACT
            Contract testGetContractByContractIDResult = testContractLogic.GetContractByID(contractid);

            // ASSERT
            Assert.That(testGetContractByContractIDResult, Is.EqualTo(this.mockGenerator.TestContractList.Single(test => test.ContractID == contractid)));
        }
    }
}
