﻿// <copyright file="IBbsCustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using DatabaseClassLibrary;

    /// <summary>
    /// Interface of the BbsCustomerLogic.
    /// </summary>
    public interface IBbsCustomerLogic
    {
        // CRUD - Create, Read, Update, Delete

        // CREATE

        /// <summary>
        /// Create one new customer.
        /// </summary>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        string CreateCustomer(string name, string address, string birthday, string accnum, string salary);

        // Read

        /// <summary>
        /// Get all customer from the repository.
        /// </summary>
        /// <returns> One customer collection. </returns>
        IEnumerable<Customer> GetAllCustomers();

        /// <summary>
        /// Get one customer by ID from the repository.
        /// </summary>
        /// <param name="custid"> The ID of customer. </param>
        /// <returns> The searched customer. </returns>
        Customer GetCustByID(int custid);

        /// <summary>
        /// Get all customer by customer name from repository.
        /// </summary>
        /// <param name="custname"> The name of the customer. </param>
        /// <returns> Contract collection by name. </returns>
        IEnumerable<Customer> GetCustByName(string custname);

        // Update

        /// <summary>
        /// Update the selected customer.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <param name="name"> The name of the customer. </param>
        /// <param name="address"> The address of the customer. </param>
        /// <param name="birthday"> The birthday of the customer. </param>
        /// <param name="accnum"> The account number of the customer. </param>
        /// <param name="salary"> The salary of the customer. </param>
        /// <returns> If update was succesfull, the return null, else return error message. </returns>
        string UpdateCustomer(int custid, string name, string address, string birthday, string accnum, string salary);

        // Delete - only logical delete for customer

        /// <summary>
        /// Delete the selected customer.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <returns> If delete was succesfull, the return true, else return false. </returns>
        bool DeleteCustomer(int custid);
    }
}
