﻿// <copyright file="IBbsUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using DatabaseClassLibrary;

    /// <summary>
    /// Interface of the BbsUserLogic.
    /// </summary>
    public interface IBbsUserLogic
    {
        // CRUD - Create, Read, Update, Delete (not available)

        // CREATE

        /// <summary>
        /// Create new user.
        /// </summary>
        /// <param name="name"> The name of the user. </param>
        /// <param name="password"> The password of the user. </param>
        /// <param name="hiredate"> The hiredate of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The addresss of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        string CreateUser(string name, string password, string hiredate, string salary, string phone, string address, string isboss);

        // Read

        /// <summary>
        /// Get all user from the repository.
        /// </summary>
        /// <returns> One user collection. </returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Get one user by ID from the repository.
        /// </summary>
        /// <param name="userid"> The ID of user. </param>
        /// <returns> The searched user. </returns>
        User GetUserByID(int userid);

        /// <summary>
        /// Get all user by user name from repository.
        /// </summary>
        /// <param name="username"> The name of the user. </param>
        /// <returns> User collection by name. </returns>
        IEnumerable<User> GetUserByName(string username);

        // Update

        /// <summary>
        /// Update the selected user.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="name"> The name of the user. </param>
        /// <param name="password"> The password of the user. </param>
        /// <param name="salary"> The salary of the user. </param>
        /// <param name="phone"> The phone number of the user. </param>
        /// <param name="address"> The addresss of the user. </param>
        /// <param name="isboss"> The job title of the user. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        string UpdateUser(int userid, string name, string password, string salary, string phone, string address, string isboss);

        /// <summary>
        /// Login with id and password.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="password"> The password of the user. </param>
        /// <returns> If the id was incorrect return -1, else if the password was incorrect return 0, else return 1. </returns>
        int LoginUser(int userid, string password);

        /// <summary>
        /// Creates chart data, for each user the total amount of contracts made.
        /// </summary>
        /// <returns> Chart data. </returns>
        List<KeyValuePair<string, int>> GetUserChartData();
    }
}
