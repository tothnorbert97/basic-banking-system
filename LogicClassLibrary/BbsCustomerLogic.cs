﻿// <copyright file="BbsCustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;

    /// <summary>
    /// Customer type subclass of the BbsLogic.
    /// </summary>
    public class BbsCustomerLogic : BbsLogic, IBbsCustomerLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BbsCustomerLogic"/> class.
        /// </summary>
        /// <param name="repository"> Data repository. </param>
        public BbsCustomerLogic(IBbsRepo repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="name"> Name. </param>
        /// <param name="address"> Address. </param>
        /// <param name="birthday"> Birth Date. </param>
        /// <param name="accnum"> Account number. </param>
        /// <param name="salary"> Salary. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        public string CreateCustomer(string name, string address, string birthday, string accnum, string salary)
        {
            string error_message = this.ErrorMessage(name, address, birthday, accnum, salary);
            if (error_message == null)
            {
                Customer newCust = new Customer()
                {
                    CStatus = " ",
                    CustomerID = this.DataRepository.GetAllCustomer().ToList().Count + 1,
                    Name = name,
                    Addr = address,
                    BirthDate = DateTime.Parse(birthday),
                    AccNum = accnum,
                    Salary = int.Parse(salary),
                };
                this.DataRepository.AddCustomerToDatabase(newCust);
                return null;
            }
            else
            {
                return error_message;
            }
        }

        /// <summary>
        /// Logical delete of customer.
        /// </summary>
        /// <param name="custid"> Customer ID. </param>
        /// <returns> Returns true if deletion was succesful. </returns>
        public bool DeleteCustomer(int custid)
        {
            // First validate input parameter
            if (custid < 1)
            {
                return false;
            }

            // Logical delete -> Update, set Status to 'C' (closed)
            var del_Cust = this.DataRepository.GetAllCustomer().Single(cust => cust.CustomerID == custid);

            del_Cust.CStatus = "C";

            try
            {
                // Call method to update database
                this.DataRepository.UpdateDatabase();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Get all the customers from the database
        /// </summary>
        /// <returns>List of customers</returns>
        public IEnumerable<Customer> GetAllCustomers()
        {
            return this.DataRepository.GetAllCustomer().Where(cst => cst.CStatus != "C");
        }

        /// <summary>
        /// Get Customer based on ID
        /// </summary>
        /// <param name="custid">Customer ID</param>
        /// <returns>Returns the customer</returns>
        public Customer GetCustByID(int custid)
        {
            if (custid < 1)
            {
                return null;
            }

            var cust = this.DataRepository.GetAllCustomer().SingleOrDefault(cst => cst.CustomerID == custid && cst.CStatus != "C");
            return cust;
        }

        /// <summary>
        /// Get all customers which contains the input string (name)
        /// </summary>
        /// <param name="custname">Customer name</param>
        /// <returns>List of customers which name contains the input string</returns>
        public IEnumerable<Customer> GetCustByName(string custname)
        {
            var cust_matches = this.DataRepository.GetAllCustomer().Where(cst => cst.Name.ToLower().Contains(custname.ToLower()) && cst.CStatus != "C");
            return cust_matches;
        }

        /// <summary>
        /// Updates the selected customer
        /// </summary>
        /// <param name="custid">Customer ID</param>
        /// <param name="name">Name</param>
        /// <param name="address">Address</param>
        /// <param name="birthday">Birth date</param>
        /// <param name="accnum">Account number</param>
        /// <param name="salary">Salary</param>
        /// <returns>Returns true if customer update was succesful</returns>
        public string UpdateCustomer(int custid, string name, string address, string birthday, string accnum, string salary)
        {
            string error_message = this.ErrorMessage(name, address, birthday, accnum, salary);
            if (error_message == null)
            {
                Customer cust_upd = this.GetCustByID(custid);
                cust_upd.Name = name;
                cust_upd.Addr = address;
                cust_upd.BirthDate = DateTime.Parse(birthday);
                cust_upd.AccNum = accnum;
                cust_upd.Salary = int.Parse(salary);
                this.DataRepository.UpdateDatabase();
                return null;
            }
            else
            {
                return error_message;
            }
        }

        private string ErrorMessage(string name, string address, string birthday, string accnum, string salary)
        {
            if (string.IsNullOrEmpty(name))
            {
                return "You don't given a name!";
            }

            if (name.Length > 40)
            {
                return "You given too long name!";
            }

            if (string.IsNullOrEmpty(address))
            {
                return "You don't given an address!";
            }

            if (address.Length > 70)
            {
                return "You given too long address!";
            }

            if (string.IsNullOrEmpty(birthday))
            {
                return "You don't given a birthday!";
            }

            DateTime parsed_birthday;
            if (!DateTime.TryParse(birthday, out parsed_birthday))
            {
                return "You given invalid birthday!";
            }

            if (string.IsNullOrEmpty(accnum))
            {
                return "You don't given an account number!";
            }

            if (accnum.Length > 24)
            {
                return "You given too long account number!";
            }

            if (string.IsNullOrEmpty(salary))
            {
                return "You don't given a salary!";
            }

            int parsed_salary;
            if (!int.TryParse(salary, out parsed_salary))
            {
                return "The salary can contain only digits!";
            }

            if (parsed_salary > 100000000)
            {
                return "You given too much salary!";
            }

            if (parsed_salary < 1)
            {
                return "You given too little salary!";
            }

            return null;
        }
    }
}
