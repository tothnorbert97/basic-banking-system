﻿// <copyright file="BbsLoanLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;

    /// <summary>
    /// Loan type subclass of the BbsLogic.
    /// </summary>
    public class BbsLoanLogic : BbsLogic, IBbsLoanLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BbsLoanLogic"/> class.
        /// Constructor
        /// </summary>
        /// <param name="repository">Data repository</param>
        public BbsLoanLogic(IBbsRepo repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Creates new loan/product
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="descr">Description</param>
        /// <param name="maxamount">Maximum available amount</param>
        /// <param name="maxdur">Maximum duration in month</param>
        /// <param name="minsal">Minimum required salary for this loan type</param>
        /// <param name="loanapr">Loan APR</param>
        /// <returns>Returns tre if loan creaton was succesful</returns>
        public string CreateLoan(string name, string descr, string maxamount, string maxdur, string minsal, string loanapr)
        {
            string error_message = this.ErrorMessage(name, descr, maxamount, maxdur, minsal, loanapr);
            if (error_message == null)
            {
                Loan newLoan = new Loan()
                {
                    ProductID = this.GetAllLoans().ToList().Count + 1,
                    Name = name,
                    Descr = descr,
                    MaxAmount = int.Parse(maxamount),
                    MaxDur = int.Parse(maxdur),
                    MinSal = int.Parse(minsal),
                    LoanAPR = int.Parse(loanapr)
                };
                this.DataRepository.AddLoanToDatabase(newLoan);
                return null;
            }
            else
            {
                return error_message;
            }
        }

        /// <summary>
        /// Get all the loan types in the database
        /// </summary>
        /// <returns>List of loans</returns>
        public IEnumerable<Loan> GetAllLoans()
        {
            return this.DataRepository.GetAllLoan();
        }

        /// <summary>
        /// Get loan by product ID
        /// </summary>
        /// <param name="productid">Product ID</param>
        /// <returns>Loan if found</returns>
        public Loan GetLoanByID(int productid)
        {
            var loan = this.DataRepository.GetAllLoan().SingleOrDefault(ln => ln.ProductID == productid);
            return loan;
        }

        /// <summary>
        /// Get loan by name
        /// </summary>
        /// <param name="prodname">Exact loan name</param>
        /// <returns>Loans which contains string</returns>
        public IEnumerable<Loan> GetLoanByName(string prodname)
        {
            var loans = this.DataRepository.GetAllLoan().Where(ln => ln.Name.ToLower().Contains(prodname.ToLower()));
            return loans;
        }

        /// <summary>
        /// Update an existing loan
        /// </summary>
        /// <param name="productid">Product ID</param>
        /// <param name="name">Name</param>
        /// <param name="descr">Description</param>
        /// <param name="maxamount">Max amount</param>
        /// <param name="maxdur">Max duration</param>
        /// <param name="minsal">Min salary</param>
        /// <param name="loanapr">LoanAPR</param>
        /// <returns>Returns true if loan update was succesful</returns>
        public string UpdateLoan(int productid, string name, string descr, string maxamount, string maxdur, string minsal, string loanapr)
        {
            string error_message = this.ErrorMessage(name, descr, maxamount, maxdur, minsal, loanapr);
            if (error_message == null)
            {
                Loan loan_upd = this.GetLoanByID(productid);
                loan_upd.Name = name;
                loan_upd.Descr = descr;
                loan_upd.MaxAmount = int.Parse(maxamount);
                loan_upd.MaxDur = int.Parse(maxdur);
                loan_upd.MinSal = int.Parse(minsal);
                loan_upd.LoanAPR = int.Parse(loanapr);
                this.DataRepository.UpdateDatabase();
                return null;
            }
            else
            {
                return error_message;
            }
        }

        private string ErrorMessage(string name, string descr, string maxamount, string maxdur, string minsal, string loanapr)
        {
            if (string.IsNullOrEmpty(name))
            {
                return "You don't given a name!";
            }

            if (name.Length > 20)
            {
                return "You given too long name!";
            }

            if (string.IsNullOrEmpty(descr))
            {
                return "You don't given a description!";
            }

            if (descr.Length > 100)
            {
                return "You given too long description!";
            }

            if (string.IsNullOrEmpty(maxamount))
            {
                return "You don't given a maximum amount!";
            }

            int parsed_maxamount;
            if (!int.TryParse(maxamount, out parsed_maxamount))
            {
                return "The maximum amount can contain only digits!";
            }

            if (parsed_maxamount > 300000000)
            {
                return "You given too much maximum amount!";
            }

            if (parsed_maxamount < 1)
            {
                return "You given too little maximum amount!";
            }

            if (string.IsNullOrEmpty(maxdur))
            {
                return "You don't given a maximum duration!";
            }

            int parsed_maxdur;
            if (!int.TryParse(maxdur, out parsed_maxdur))
            {
                return "The maximum duration can contain only digits!";
            }

            if (parsed_maxdur > 999)
            {
                return "You given too much maximum duration!";
            }

            if (parsed_maxdur < 1)
            {
                return "You given too little maximum duration!";
            }

            if (string.IsNullOrEmpty(minsal))
            {
                return "You don't given a minimum salary!";
            }

            int parsed_minsal;
            if (!int.TryParse(minsal, out parsed_minsal))
            {
                return "The minimum salary can contain only digits!";
            }

            if (parsed_minsal > 10000000)
            {
                return "You given too much minimum salary!";
            }

            if (parsed_minsal < 1)
            {
                return "You given too little minimum salary!";
            }

            if (string.IsNullOrEmpty(loanapr))
            {
                return "You don't given an APR!";
            }

            int parsed_apr;
            if (!int.TryParse(loanapr, out parsed_apr))
            {
                return "The APR can contain only digits!";
            }

            if (parsed_apr > 100)
            {
                return "You given too much APR!";
            }

            if (parsed_apr < 1)
            {
                return "You given too little APR!";
            }

            return null;
        }
    }
}
