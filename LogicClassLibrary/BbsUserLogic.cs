﻿// <copyright file="BbsUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using DatabaseClassLibrary;

    /// <summary>
    /// User type subclass of the BbsLogic.
    /// </summary>
    public class BbsUserLogic : BbsLogic, IBbsUserLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BbsUserLogic"/> class.
        /// Constructor
        /// </summary>
        /// <param name="repository"> Data repository</param>
        public BbsUserLogic(IBbsRepo repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Create new user entity
        /// </summary>
        /// <param name="name"> Users name</param>
        /// <param name="password">Users password</param>
        /// <param name="hiredate"> Hiredate </param>
        /// <param name="salary">Salary</param>
        /// <param name="phone">Phone</param>
        /// <param name="address">Address</param>
        /// <param name="isboss"> If user is boss Y else N</param>
        /// <returns> Returns true if creation was succesful</returns>
        public string CreateUser(string name, string password, string hiredate, string salary, string phone, string address, string isboss)
        {
            string error_message = this.ErrorMessage(true, name, password, hiredate, salary, phone, address);
            if (error_message == null)
            {
                // Create new user using the parameters
                User newUsr = new User()
                {
                    UserID = this.DataRepository.GetAllUser().Count() + 1,
                    UserPW = this.HashUserPW(password),
                    Name = name,
                    HireDate = DateTime.Parse(hiredate),
                    Salary = int.Parse(salary),
                    Phone = phone,
                    Addr = address,
                    IsBoss = isboss
                };
                this.DataRepository.AddUserToDatabase(newUsr);
                return null;
            }
            else
            {
                return error_message;
            }
        }

        /// <summary>
        /// Get all the users from database
        /// </summary>
        /// <returns>List of users</returns>
        public IEnumerable<User> GetAllUsers()
        {
            return this.DataRepository.GetAllUser();
        }

        /// <summary>
        /// Get user by ID
        /// </summary>
        /// <param name="userid">User ID</param>
        /// <returns>The user</returns>
        public User GetUserByID(int userid)
        {
            var user = this.DataRepository.GetAllUser().SingleOrDefault(usr => usr.UserID == userid);
            return user;
        }

        /// <summary>
        /// Get all users which contains the input string (name)
        /// </summary>
        /// <param name="username">User name</param>
        /// <returns>List of users which name contains the input string</returns>
        public IEnumerable<User> GetUserByName(string username)
        {
            var usr_matches = this.DataRepository.GetAllUser().Where(usr => usr.Name.ToLower().Contains(username.ToLower()));
            return usr_matches;
        }

        /// <summary>
        /// Login user method
        /// </summary>
        /// <param name="userid">UserID</param>
        /// <param name="password">Password</param>
        /// <returns>Returns -1 if user ID is wrong, returns 0 if password is wrong, returns 1 if login succesful</returns>
        public int LoginUser(int userid, string password)
        {
            User user = this.GetUserByID(userid);

            if (user != null)
            {
                if (password != null && user.UserPW == this.HashUserPW(password))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Updates an User based on the Userid
        /// </summary>
        /// <param name="userid">userid</param>
        /// <param name="name">name</param>
        /// <param name="password">password</param>
        /// <param name="salary">salary</param>
        /// <param name="phone">phone</param>
        /// <param name="address">address</param>
        /// <param name="isboss">If user is boss Y else N</param>
        /// <returns>Returns true if update was succesful</returns>
        public string UpdateUser(int userid, string name, string password, string salary, string phone, string address, string isboss)
        {
            string error_message = this.ErrorMessage(false, name, password, null, salary, phone, address);
            if (error_message == null)
            {
                User user_upd = this.GetUserByID(userid);
                user_upd.UserPW = password;
                user_upd.Name = name;
                user_upd.Salary = int.Parse(salary);
                user_upd.Phone = phone;
                user_upd.Addr = address;
                user_upd.IsBoss = isboss;
                this.DataRepository.UpdateDatabase();
                return null;
            }
            else
            {
                return error_message;
            }
        }

        /// <summary>
        /// Creates chart data, for each user the total amount of contracts made
        /// </summary>
        /// <returns> Chart data</returns>
        public List<KeyValuePair<string, int>> GetUserChartData()
        {
            var chart_data_q = this.DataRepository.GetAllContract().GroupBy(ctr => ctr.UserID).Select(g => new { UsrID = g.Key, Contractmoney = g.Sum(ctr => ctr.Amount) });

            List<KeyValuePair<string, int>> chart_data = new List<KeyValuePair<string, int>>();

            foreach (var item in chart_data_q)
            {
                // chart_data.Add(new KeyValuePair<string, int>(item.Name.ToString(), (int)item.Contractmoney));
                var usr_name = this.DataRepository.GetAllUser().Where(usr => usr.UserID == item.UsrID).Select(usr => usr.Name).Single().ToString();
                chart_data.Add(new KeyValuePair<string, int>(usr_name, (int)item.Contractmoney));
            }

            return chart_data;
        }

        /// <summary>
        /// Creates Hash from user password
        /// </summary>
        /// <param name="password"> User password string</param>
        /// <returns>Hash of user password string</returns>
        public string HashUserPW(string password)
        {
            var sha256 = new SHA256Managed();

            // Send a sample text to hash
            var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

            // Get the hashed string
            string hash = BitConverter.ToString(hashedBytes).Replace("-", string.Empty).ToLower();

            // Print the string.
            return hash;
        }

        private string ErrorMessage(bool add, string name, string password, string hiredate, string salary, string phone_number, string address)
        {
            if (string.IsNullOrEmpty(name))
            {
                return "You don't given a name!";
            }

            if (name.Length > 40)
            {
                return "You given too long name!";
            }

            if (add && string.IsNullOrEmpty(password))
            {
                return "You don't given a password!";
            }

            if (add && password.Length > 64)
            {
                return "You given too long password!";
            }

            if (add && string.IsNullOrEmpty(hiredate))
            {
                return "You don't given a hiredate!";
            }

            DateTime parsed_hiredate;
            if (add && !DateTime.TryParse(hiredate, out parsed_hiredate))
            {
                return "You given invalid hiredate!";
            }

            if (string.IsNullOrEmpty(salary))
            {
                return "You don't given a salary!";
            }

            int parsed_salary;
            if (!int.TryParse(salary, out parsed_salary))
            {
                return "The salary can contain only digits!";
            }

            if (parsed_salary > 100000000)
            {
                return "You given too much salary!";
            }

            if (parsed_salary < 1)
            {
                return "You given too little salary!";
            }

            if (string.IsNullOrEmpty(phone_number))
            {
                return "You don't given a phone number!";
            }

            if (phone_number.Length > 12)
            {
                return "You given too long phone number!";
            }

            if (phone_number.Length < 12)
            {
                return "You given too short phone number!";
            }

            if (string.IsNullOrEmpty(address))
            {
                return "You don't given an address!";
            }

            if (address.Length > 70)
            {
                return "You given too long address!";
            }

            return null;
        }
    }
}
