﻿// <copyright file="BbsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using DatabaseClassLibrary;

    /// <summary>
    /// Logic parent class.
    /// </summary>
    public class BbsLogic
    {
        /// <summary>
        /// Interface of the repository.
        /// </summary>
        private IBbsRepo dataRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BbsLogic"/> class.
        /// </summary>
        /// <param name="repository"> The used repository. </param>
        public BbsLogic(IBbsRepo repository)
        {
            this.DataRepository = repository;
        }

        /// <summary>
        /// Gets or sets the dataRepository.
        /// </summary>
        public IBbsRepo DataRepository
        {
            get
            {
                return this.dataRepository;
            }

            set
            {
                this.dataRepository = value;
            }
        }
    }
}
