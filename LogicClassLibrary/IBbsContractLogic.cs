﻿// <copyright file="IBbsContractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System.Collections.Generic;
    using DatabaseClassLibrary;

    /// <summary>
    /// Interface of the BbsContractLogic.
    /// </summary>
    public interface IBbsContractLogic
    {
        // CRUD - Create, Read, (Update, Delete - not available)

        // Create

        /// <summary>
        /// Create one new contract.
        /// </summary>
        /// <param name="custid"> The name of the contract. </param>
        /// <param name="productid"> The ID of the product. </param>
        /// <param name="userid"> The ID of the user. </param>
        /// <param name="amount"> The amount of the contract. </param>
        /// <param name="duration"> The duration of the contract. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        string CreateContract(string custid, string productid, int userid, string amount, string duration);

        // Read

        /// <summary>
        /// Get all contract from the repository.
        /// </summary>
        /// <returns> One contract collection. </returns>
        IEnumerable<Contract> GetAllContracts();

        /// <summary>
        /// Get one contract by ID from the repository.
        /// </summary>
        /// <param name="contractid"> The ID of contract </param>
        /// <returns> The searched contract. </returns>
        Contract GetContractByID(int contractid);

        /// <summary>
        /// Get all contracts by customer ID from repository.
        /// </summary>
        /// <param name="custid"> The ID of the customer. </param>
        /// <returns> Contract collection of the customer. </returns>
        IEnumerable<Contract> GetContractsByCustomer(int custid);

        /// <summary>
        /// Get all contracts by product ID from repository.
        /// </summary>
        /// <param name="productid"> The ID of the product. </param>
        /// <returns> Contract collection of the product. </returns>
        IEnumerable<Contract> GetContractsByProduct(int productid);

        /// <summary>
        /// Get all contract by user ID from repository.
        /// </summary>
        /// <param name="userid"> The ID of the user. </param>
        /// <returns> Contract collection of the user. </returns>
        IEnumerable<Contract> GetContractsByUser(int userid);
    }
}
