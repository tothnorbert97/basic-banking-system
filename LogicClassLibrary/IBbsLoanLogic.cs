﻿// <copyright file="IBbsLoanLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System.Collections.Generic;
    using DatabaseClassLibrary;

    /// <summary>
    /// Interface of the BbsLoanLogic.
    /// </summary>
    public interface IBbsLoanLogic
    {
        // CRUD - Create, Read, Update, Delete(not available)

        // Create

        /// <summary>
        /// Create one new loan.
        /// </summary>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="descr"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The maximum duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the customer. </param>
        /// <param name="loanapr"> The annual percentage rate of the loan. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        string CreateLoan(string name, string descr, string maxamount, string maxdur, string minsal, string loanapr);

        // Read

        /// <summary>
        /// Get all loan from the repository.
        /// </summary>
        /// <returns> One loan collection. </returns>
        IEnumerable<Loan> GetAllLoans();

        /// <summary>
        /// Get one loan by ID from the repository.
        /// </summary>
        /// <param name="productid"> The ID of loan. </param>
        /// <returns> The searched loan. </returns>
        Loan GetLoanByID(int productid);

        /// <summary>
        /// Get all loan by loan name from repository.
        /// </summary>
        /// <param name="prodname"> The name of the loan. </param>
        /// <returns> Loan collection by name. </returns>
        IEnumerable<Loan> GetLoanByName(string prodname);

        // Update

        /// <summary>
        /// Update the selected loan.
        /// </summary>
        /// <param name="productid"> The ID of the loan. </param>
        /// <param name="name"> The name of the loan. </param>
        /// <param name="descr"> The description of the loan. </param>
        /// <param name="maxamount"> The maximum amount of the loan. </param>
        /// <param name="maxdur"> The maximum duration of the loan. </param>
        /// <param name="minsal"> The minimum salary of the customer. </param>
        /// <param name="loanapr"> The annual percentage rate of the loan. </param>
        /// <returns> If update was succesfull, the return null, else return error message. </returns>
        string UpdateLoan(int productid, string name, string descr, string maxamount, string maxdur, string minsal, string loanapr);
    }
}
