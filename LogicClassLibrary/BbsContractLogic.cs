﻿// <copyright file="BbsContractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LogicClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseClassLibrary;

    /// <summary>
    /// Contract type subclass of the BbsLogic.
    /// </summary>
    public class BbsContractLogic : BbsLogic, IBbsContractLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BbsContractLogic"/> class.
        /// </summary>
        /// <param name="repository">Data repository</param>
        public BbsContractLogic(IBbsRepo repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Create new contract
        /// </summary>
        /// <param name="custid"> Customer ID of contract person. </param>
        /// <param name="productid"> Product ID of conract. </param>
        /// <param name="userid"> Contract supervisor ID. </param>
        /// <param name="amount"> Amount. </param>
        /// <param name="duration"> Duration. </param>
        /// <returns> If create was succesfull, the return null, else return error message. </returns>
        public string CreateContract(string custid, string productid, int userid, string amount, string duration)
        {
            string error_message = this.ErrorMessage(userid, custid, productid, amount, duration);
            if (error_message == null)
            {
                Contract newContract = new Contract()
                {
                    ContractID = this.GetAllContracts().Count() + 1,
                    CustomerID = int.Parse(custid),
                    ProductID = int.Parse(productid),
                    UserID = userid,
                    CreationDate = DateTime.Now,
                    Amount = int.Parse(amount),
                    Duration = int.Parse(duration),
                    Name = this.GetAllContracts().Count() + 1
                };
                this.DataRepository.AddContractToDatabase(newContract);
                return null;
            }
            else
            {
                return error_message;
            }
        }

        /// <summary>
        /// Get all the contracts.
        /// </summary>
        /// <returns> List of all the Contracts. </returns>
        public IEnumerable<Contract> GetAllContracts()
        {
            return this.DataRepository.GetAllContract();
        }

        /// <summary>
        /// Get all the Contracts of a customer.
        /// </summary>
        /// <param name="custid"> Customer id. </param>
        /// <returns> List of contracts. </returns>
        public IEnumerable<Contract> GetContractsByCustomer(int custid)
        {
            return this.DataRepository.GetAllContract().Where(r => r.CustomerID == custid);
        }

        /// <summary>
        /// Get all the Contracts which has the same loan type.
        /// </summary>
        /// <param name="productid"> Product ID. </param>
        /// <returns> List of contracts. </returns>
        public IEnumerable<Contract> GetContractsByProduct(int productid)
        {
            return this.DataRepository.GetAllContract().Where(r => r.ProductID == productid);
        }

        /// <summary>
        /// Get all the Contracts which are supervised by a particular user.
        /// </summary>
        /// <param name="userid"> User ID. </param>
        /// <returns> List of Contracts. </returns>
        public IEnumerable<Contract> GetContractsByUser(int userid)
        {
            return this.DataRepository.GetAllContract().Where(r => r.UserID == userid);
        }

        /// <summary>
        /// Get Contract by Contract ID.
        /// </summary>
        /// <param name="contractid"> Contract ID. </param>
        /// <returns> List of contracts. </returns>
        public Contract GetContractByID(int contractid)
        {
            return this.DataRepository.GetAllContract().SingleOrDefault(r => r.ContractID == contractid);
        }

        private string ErrorMessage(int userid, string custid, string productid, string amount, string duration)
        {
            if (string.IsNullOrEmpty(custid))
            {
                return "You don't given a customer id!";
            }

            int parsed_custid;
            if (!int.TryParse(custid, out parsed_custid))
            {
                return "The customer ID can contain only digits!";
            }

            Customer customer = new BbsCustomerLogic(this.DataRepository).GetCustByID(parsed_custid);

            if (parsed_custid > new BbsCustomerLogic(this.DataRepository).GetAllCustomers().Count())
            {
                return "You given too much customer ID!";
            }

            if (parsed_custid < 1)
            {
                return "You given too little customer ID!";
            }

            if (string.IsNullOrEmpty(productid))
            {
                return "You don't given a product id!";
            }

            int parsed_productid;
            if (!int.TryParse(productid, out parsed_productid))
            {
                return "The product ID can contain only digits!";
            }

            Loan loan = new BbsLoanLogic(this.DataRepository).GetLoanByID(parsed_productid);

            if (parsed_productid > new BbsLoanLogic(this.DataRepository).GetAllLoans().Count())
            {
                return "You given too much product ID!";
            }

            if (parsed_productid < 1)
            {
                return "You given too little product ID!";
            }

            if (string.IsNullOrEmpty(amount))
            {
                return "You don't given an amount!";
            }

            int parsed_amount;
            if (!int.TryParse(amount, out parsed_amount))
            {
                return "The amount can contain only digits!";
            }

            if (parsed_amount > 1200000000)
            {
                return "You given too much amount!";
            }

            if (parsed_amount < 1)
            {
                return "You given too little amount!";
            }

            if (parsed_amount > loan.MaxAmount)
            {
                return "You given higher amount than selected loan's maximum amount!";
            }

            if (string.IsNullOrEmpty(duration))
            {
                return "You don't given a duration!";
            }

            int parsed_duration;
            if (!int.TryParse(duration, out parsed_duration))
            {
                return "The duration can contain only digits!";
            }

            if (parsed_duration > 999)
            {
                return "You given too much duration";
            }

            if (parsed_duration < 1)
            {
                return "You given too little duration!";
            }

            if (parsed_duration > loan.MaxDur)
            {
                return "You given higher duration than the selected loan's maximum duration!";
            }

            if (customer.Salary < loan.MinSal)
            {
                return "Your selected loan's minimum salary higher than the selected customer's salary";
            }

            return null;
        }
    }
}
